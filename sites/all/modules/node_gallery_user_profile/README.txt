$Id: README.txt,v 1.1.2.1 2010/12/22 22:34:23 justintime Exp $

Summary
-------

Node Gallery User Profile is an add-on module to Node Gallery that provides a
tab on each user's profile page labeled "My Galleries". By clicking the tab,
other users may see galleries authored by the user they are currently viewing.
The content on the tab itself is a view, so there are limitless possibilities
for the admin to configure. A default view is supplied that will suit most
sites.

Installation
------------
 * Copy the module file to your modules directory, extract it, and enable it at
/admin/build/modules.
 
 * At this point, you're most likely done.  Vist a user's profile page, and
click the tab labeled "My Galleries".

Customization
-------------
 * Visit admin/settings/node_gallery/settings and select the display you wish 
to use by changing the 'View display to use on the "My Galleries" tab...'
checkbox.

 * Only users granted the 'view node gallery' permission can view the tab on 
the user profile.
