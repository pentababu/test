---- CHANGELOG ---

Beta3:
. Picture is a link to the account page
. Add user_titles support
. Add Login or Register for Anonymous users

Beta2:
. add base_path. 
. add welcome title, optionally logout link
. user points and badges if these modules are active
. add a changelog :-)

Beta1: first version

