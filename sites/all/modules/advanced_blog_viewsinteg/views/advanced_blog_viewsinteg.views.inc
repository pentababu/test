<?php

function advanced_blog_viewsinteg_views_data() {
  $data['advanced_blog']['table']['group'] = t('Advanced Blog');
  $data['advanced_blog']['table']['base'] = array(
    'field' => 'uid', 
    'title' => t('Advanced Blog table'), 
    'help' => t('Maps to uid in user table.'), 
    'weight' => -10,
  );
  $data['advanced_blog']['table']['join'] = array(    
    'users' => array(
      'left_field' => 'uid', 
      'field' => 'uid',
    ),
  );
  // uid field
  $data['advanced_blog']['uid'] = array(
    'title' => t('User id'), 
    'help' => t('Unique id of user'),
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => t('TRUE'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),    
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // Blog Title field
  $data['advanced_blog']['title'] = array(
    'title' => t('Blog Title'), 
    'help' => t('Blog Title of user.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ), 
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  // Blog Description field.
  $data['advanced_blog']['description'] = array(
    'title' => t('Blog Description'), 
    'help' => t('Blog Description of user.'), 
    'field' => array(
      'handler' => 'views_handler_field', 
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  return $data;
}
