// $Id$

/**
 * @file
 * Override Drupal tabledrag methods to add support for nested fieldgroups.
 */

/**
 * Adjust the classes associated with a row's weight when it is dropped.
 * Use the parent's name to base the change on.
 */
Drupal.tableDrag.prototype.onDrop = function() {
  tableDragObject = this;
  var siblingRowUp = $(tableDragObject.rowObject.element).prev('tr');
  var siblingRowsDown = $(tableDragObject.rowObject.element).nextAll('tr');
  var weightField = $('input.field-weight', tableDragObject.rowObject.element);
  var siblingWeightFieldDown = $('input.field-weight', siblingRowsDown);
  var oldWeightField = $('input.field-weight', tableDragObject.rowObject.element);
  var previousRow = $(tableDragObject.rowObject.element).prev('tr');


  while (previousRow.length && $('.indentation', previousRow).length >= this.rowObject.indents) {
    previousRow = previousRow.prev('tr');
  }
  // If we found a row.
  if (previousRow.length) {
    sourceRow = previousRow[0];
    var oldGroupField = $('input.field-name', sourceRow);
    var newGroupName = oldGroupField[0].value.replace(/_/g, '-');
  }
  // Otherwise we went all the way to the left of the table without finding
  // a parent, meaning this item has been placed at the root level.
  else {
    var newGroupName = "top";
  }

  var oldGroupName = oldWeightField[0].className.replace(/([^ ]+[ ]+)*field-weight-([^ ]+)([ ]+[^ ]+)*/, '$2');
  var oldClass = 'field-weight-' + oldGroupName;
  var newClass = 'field-weight-' + newGroupName;
  weightField.removeClass(oldClass).addClass(newClass);

  //Now that we've updated the class names, let's make sure the weight is properly set.
  //First make sure that if the rows above are nested deeper, that we find their parent, which is a sibling of
  //our current row.
  while (siblingRowUp.length && $('.indentation', siblingRowUp).length > this.rowObject.indents) {
    siblingRowUp = siblingRowUp.prev('tr');
  }
  //before we set the new weight, make sure we know whether the previous row is a sibling or a parent
  var siblingIndent = $('.indentation', siblingRowUp).length;
  var indents = this.rowObject.indents;

  //if we found a sibling
  if (siblingIndent == indents) {
    //the previous row is a sibling, so let's set the new weight
    var siblingWeightFieldUp = $('input.field-weight', siblingRowUp);
    if (siblingWeightFieldUp[0] != undefined){
      var siblingWeightUp = siblingWeightFieldUp[0].value;
      var newWeight = parseInt(siblingWeightUp) + 1;
      $('input.field-weight', tableDragObject.rowObject.element).val(newWeight);
    }

    //now deal with the case where we moved left/right
    if (oldClass != newClass) {
      $(tableDragObject.rowObject.element).nextAll('tr').each(function() {
        if ($('input.field-weight', this).hasClass(newClass)) {
          //let's make sure we grab an actual sibling
          var prevRow = $(this).prev('tr');
          while (prevRow.length && $('.indentation', prevRow).length > $('.indentation', this).length) {
            prevRow = prevRow.prev('tr');
          }
          var prevWeight = $('input.field-weight', prevRow).val();
          var incWeight = parseInt(prevWeight) + 1;
          $('input.field-weight', this).val(incWeight);
        }
      });
    }
  }
  else {
    //the previous row is a parent, which means we're at the top of this group, so set the index to zero
    $('input.field-weight', tableDragObject.rowObject.element).val(0);

    //find the row's group to pass in to the each function for comparison since we only want to affect the
    //weights in this same group
    $(tableDragObject.rowObject.element).nextAll('tr').each(function() {
      if ($('input.field-weight', this).hasClass(newClass)) {
        //let's make sure we grab an actual sibling
        var prevRow = $(this).prev('tr');
        while (prevRow.length && $('.indentation', prevRow).length > $('.indentation', this).length) {
          prevRow = prevRow.prev('tr');
        }
        var prevWeight = $('input.field-weight', prevRow).val();
        var incWeight = parseInt(prevWeight) + 1;
        $('input.field-weight', this).val(incWeight);
      }
    });
  }
}

/**
* Determine the valid indentations interval for the row at a given position
* in the table.
*
* @param prevRow
*   DOM object for the row before the tested position
*   (or null for first position in the table).
* @param nextRow
*   DOM object for the row after the tested position
*   (or null for last position in the table).
*/
Drupal.tableDrag.prototype.row.prototype.validIndentInterval = function(prevRow, nextRow) {
  var minIndent, maxIndent;
  var previousRow = $(this.element).prev('tr');
  var previousMultiRow = previousRow;
  var thisRow = $(previousRow).next('tr');
  var realNextRow = $(this.element).next('tr');
  var nextMultiRow = realNextRow;
  var thisDepth = $('.indentation', thisRow).size();
  var which = 'standard';
  var rowOne, rowTwo, rowThree, rowFour;

  // Minimum indentation:
  // Do not orphan the next row.
  minIndent = nextRow ? $('.indentation', nextRow).size() : 0;

  if ($(this.element).is('.tabledrag-multigroup') || $(this.element).is('.tabledrag-standardgroup')) {
    //find the first multigroup below where we are
    while (nextMultiRow.length && (!($(nextMultiRow).is('.tabledrag-multigroup')) || ($('.indentation', nextMultiRow).size() > $('.indentation', thisRow).size()))) {
      nextMultiRow = nextMultiRow.next('tr');
    }
    if (!($(nextMultiRow).is('.tabledrag-multigroup'))) {
      nextMultiRow = null;
    }
    //find the first standard group below where we are
    while (realNextRow.length && !($(realNextRow).is('.tabledrag-standardgroup'))) {
      realNextRow = realNextRow.next('tr');
    }
    if (!($(realNextRow).is('.tabledrag-standardgroup'))) {
      realNextRow = null;
    }

    if (typeof (realNextRow) != 'undefined' && realNextRow != undefined) {
      rowOne = realNextRow.get(0);
      //alert("rowOne " + rowOne.rowIndex);
    }
    else {
      rowOne = null;
    }
    if (typeof (nextMultiRow) != 'undefined' && nextMultiRow != undefined) {
      rowTwo = nextMultiRow.get(0);
      //alert("rowTwo " + rowTwo.rowIndex);
    }
    else {
      rowTwo = null;
    }

    if (rowOne == null && rowTwo == null) {
      realNextRow = nextRow;
    }
    else if (rowTwo != null && rowOne == null) {
      realNextRow = nextMultiRow;
      which = 'multi';
    }
    else if (rowTwo != null && rowOne != null && rowTwo.rowIndex < rowOne.rowIndex) {
      realNextRow = nextMultiRow;
      which = 'multi';
    }

    //find the first multigroup above where we are at
    while (previousMultiRow.length && !($(previousMultiRow).is('.tabledrag-multigroup'))) {
      previousMultiRow = previousMultiRow.prev('tr');
    }
    if (!($(previousMultiRow).is('.tabledrag-multigroup'))) {
      previousMultiRow = null;
    }
    //find the first multigroup above where we are at
    while (previousRow.length && !($(previousRow).is('.tabledrag-standardgroup'))) {
      previousRow = previousRow.prev('tr');
    }
    if (!($(previousRow).is('.tabledrag-standardgroup'))) {
      previousRow = null;
    }

    if (typeof (previousRow) != 'undefined' && previousRow != undefined) {
      rowThree = previousRow.get(0);
      //alert("rowThree " + rowThree.rowIndex);
    }
    else {
      rowThree = null;
    }
    if (typeof (previousMultiRow) != 'undefined' && previousMultiRow != undefined) {
      rowFour = previousMultiRow.get(0);
      //alert("rowFour " + rowFour.rowIndex);
    }
    else {
      rowFour = null;
    }

    if (rowThree == null && rowFour == null) {
      previousRow = prevRow;
    }
    else if (rowFour != null && rowThree == null) {
      previousRow = previousMultiRow;
      which = 'multi';
    }
    else if (rowFour != null && rowThree != null && rowFour.rowIndex > rowThree.rowIndex) {
      previousRow = previousMultiRow;
      which = 'multi';
    }

    if ($(this.element).is('.tabledrag-multigroup') || $(this.element).is('.tabledrag-standardgroup')) {
      if (this.direction != 'down') {
        if (which == 'standard') {
          maxIndent = $('.indentation', previousRow).size() + 1;
        }
        else {
          maxIndent = $('.indentation', previousRow).size();
        }
        //alert("up " + maxIndent);
      }
      else if (this.direction == 'down') {
        if (which == 'standard') {
          maxIndent = $('.indentation', realNextRow).size() + 1;
        }
        else {
          maxIndent = $('.indentation', realNextRow).size();
        }
        //alert("down " + maxIndent);
      }
    }
  }
  else {
    // Maximum indentation:
    if (!prevRow || $(this.element).is('.tabledrag-root')) {
      // Do not indent the first row in the table or 'root' rows.
      maxIndent = 0;
    }
    else {
      // Do not go deeper than as a child of the previous row.
      maxIndent = $('.indentation', prevRow).size() + ($(prevRow).is('.tabledrag-leaf') ? 0 : 1);
      // Limit by the maximum allowed depth for the table.
      if (this.maxDepth) {
        maxIndent = Math.min(maxIndent, this.maxDepth - (this.groupDepth - this.indents));
      }
    }
  }
  //alert("min " + minIndent);
  //alert("max " + maxIndent);
  return { 'min': minIndent, 'max': maxIndent };
}
