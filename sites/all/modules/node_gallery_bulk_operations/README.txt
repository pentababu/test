$Id: README.txt,v 1.1.2.1 2010/12/22 23:26:44 justintime Exp $

Summary
-------

Node Gallery Bulk Operations extends the functionality of Node Gallery by
providing a Views Bulk Operations integration. It does this by providing a
default view that you can customize, and adds a "Bulk Operations" tab on
gallery nodes when being viewed by someone with rights to edit image nodes. It
uses batch API, so it will work on galleries with thousands of images without
additional RAM.

Installation
------------
 * Copy the module file to your modules directory, extract it, and enable it at
/admin/build/modules.
 
 * Visit your Node Gallery Relationships at admin/settings/node_gallery/list and
click "Change Settings" on each relationship you wish to enable the "Bulk
Operations" tab on.

 * Check the box labeled "Enable Bulk Operations tab on gallery node pages?",
select the view you wish to use (select 'bulk operations' if you don't know),
and press save.

 * At this point, you're most likely done.  Vist a gallery page, and
click the tab labeled "Bulk Operations".  From here you can perform various
operations to all images within the gallery at once.
