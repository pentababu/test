<?php
// $Id: advanced_profile_visitors.tpl.php,v 1.1.2.1 2010/04/05 03:34:58 michellec Exp $

/**
 * @file
 * Theme implementation to display a list of visitors to a profile.
 *
 * Available variables:
 *
 * - $visitors: List of visitors themed with theme_item_list.
 */
?>

<div class="profile-visitors">
  <?php print $visitors ?>
</div>