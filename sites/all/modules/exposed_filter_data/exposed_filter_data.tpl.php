<?php
// $Id: exposed_filter_data.tpl.php,v 1.1 2010/07/29 08:38:44 shushu Exp $

/**
 * @file
 * Basic template file
 */

?>
<div class='exposed_filter_data'>
  <div class='title'>Filtered by:</div>
  <div class='content'>
    <?php
      foreach ($exposed_filters as $filter => $value) {
        if ($value) {
          print "<div class='filter'><div class='name'>" . $filter . ":</div>";
          print "<div class='value'>" . $value . "</div></div>";
        }
      }
    ?>
  </div>
</div>
