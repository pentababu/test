// $Id: qtip.js,v 1.1 2010/08/19 06:03:47 bocaj Exp $

Drupal.behaviors.qtip = function(context) {
  /* --------------------------
       CONFIGURATION SETTINGS
     -------------------------- */
  /* Since the hiding won't function properly if 'Hide Event Type'
    is set to 'unfocus' and 'Only allow one qtip to show at a time' checkbox
    is false, we need to set Hide Event Type to 'click' so that the user has
    to click on the link text to close the qtip.
  */
  if (Drupal.settings.qtip.hide_event_type == 'unfocus' && !Drupal.settings.qtip.show_solo) {
    Drupal.settings.qtip.hide_event_type = 'click';
  }
  
  // Set delay on click to immediate
  // TODO: This might be admin-configurable in later releases
  if (Drupal.settings.qtip.show_event_type == 'click') {
    show_delay = 1;
  }
  else {
    show_delay = 140; // This is the default qTip value, set for hover links
  }
  
  // Set the proper qtip speech-bubble tip position
  if (Drupal.settings.qtip.show_side_speech_bubble_tip) {
    switch (Drupal.settings.qtip.tooltip_position) {
      case 'topLeft':
        Drupal.settings.qtip.tooltip_position = 'leftTop';
        break;
      case 'topRight':
        Drupal.settings.qtip.tooltip_position = 'topRight';
        break;
      case 'bottomRight':
        Drupal.settings.qtip.tooltip_position = 'rightBottom';
        break;
      case 'bottomLeft':
        Drupal.settings.qtip.tooltip_position = 'leftBottom';
    }
  }
  
  /* ---  END CONFIGURATION SETTINGS  --- */

  // jQuery qTip library API
  $('.qtip-link').qtip({
    content: false, // Doing this causes qTip to use the title attribute - see http://craigsworks.com/projects/qtip/docs/#replace
    position: {
      corner: {
        target: Drupal.settings.qtip.target_position,
        tooltip: Drupal.settings.qtip.tooltip_position
      }
    },
    style: { 
      tip: Drupal.settings.qtip.tooltip_position,
      border: {
        width: parseInt(Drupal.settings.qtip.border_width),
        radius: parseInt(Drupal.settings.qtip.border_radius)
      },
      name: Drupal.settings.qtip.color
    },
    show: {
      when: {
        event: Drupal.settings.qtip.show_event_type
      },
      solo: 'false',
      delay: show_delay
    },
    hide: {
      when: {
        event: Drupal.settings.qtip.hide_event_type
      }
    }
    
  })
};