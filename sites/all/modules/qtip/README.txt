-- SUMMARY --
qTip is another tooltip module for Drupal. By using a simple input filter in your
code you can have a stylish tooltip in just seconds.


-- REQUIREMENTS --
You must download and install the qTip library from
http://craigsworks.com/projects/qtip/download/
This library is licensed under MIT and therefore is not allowed to be on drupal.org

Make sure that you select the 'Production' option. You may download the development
option as well if you would like to check out the code, but this module depends
on the compressed 'Production' version of the library.

Place the extracted contents of the library into /path/to/qtip/library


-- INSTALLATION --

* Install as usual (see dependency above), see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Once installed, go to admin/settings/qtip
    * Select how you would like your qTips to display. Save.
* Go to admin/settings/filters
    * Click 'configure' on the input filter that you would like to add qTip to
      NOTE: For input filters that have 'HTML filter' enabled (like Filtered HTML), qTip MUST be weighted heavier than HTML filter
        This should be default, but it would be a good idea to check.
    * Save and repeat for as many input filters as you would like.
* On a node page enter the filter with the following format:
  [qtip:Name of link (target) text|Text to appear in tooltip]
    The filter MUST start with '[qtip:' (no quotes)
    The visible text that will always show on the node page and will be used as a link to
      the tooltip comes next, followed by | (pipe)
    Finally, the text you would like to appear in the tooltip is last, followed by ']' (no quotes)


-- MAINTAINERS --
Current maintainers:
* Jacob Neher (bocaj) - http://drupal.org/user/582042


-- SPECIAL THANKS --
To Craig Thompson, creator of the qTip jQuery plugin!
http://craigsworks.com