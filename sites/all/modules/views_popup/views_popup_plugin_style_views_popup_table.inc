<?php

module_load_include('inc','views','includes/base');
module_load_include('inc','views','includes/plugins');
module_load_include('inc','views','plugins/views_plugin_style');
module_load_include('inc','views','plugins/views_plugin_style_table');

/**
 * Admin settings callback
 */
class views_popup_plugin_style_views_popup_table extends views_plugin_style_table {
  function option_definition() {
    $options = parent::option_definition();

    $options['views_popup_exclude'] = array('default' => 1);

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['#theme'] = 'views_popup_ui_style_plugin_table';

    foreach (array_keys($form['info']) as $field ) {
      $safe = str_replace(array('][', '_', ' '), '-', $field);
      // the $id of the column for dependency checking.
      $id = 'edit-style-options-columns-' . $safe;

      if ($form['columns'][$field])
        $form['info'][$field]['popup'] = array(
            '#type' => 'select',
            '#options' => array( 0 => 'Table', 1 => 'Popup', 2 => 'Both', 3 => 'Hide' ),
            '#default_value' => $this->options['info'][$field]['popup'],
            '#process' => array('views_process_dependency'),
            '#dependency' => array($id => array($field)),
          );
    }

    $form['views_popup_class'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS Class'),
      '#default_value' => $this->options['views_popup_class'],
      '#size' => 60,
      '#description' => t('CSS class for popup.')
    );

    $form['views_popup_header'] = array(
      '#type' => 'textarea',
      '#title' => t('Header'),
      '#default_value' => $this->options['views_popup_header'],
      '#rows' => 4,
      '#wysiwyg' => FALSE,                  // disables wysiwig editors
      '#description' => t('Display at top of popup. This field may contain &lt;?php ?&gt; PHP code.')
    );

    $form['views_popup_footer'] = array(
      '#type' => 'textarea',
      '#title' => t('Footer'),
      '#default_value' => $this->options['views_popup_footer'],
      '#rows' => 4,
      '#wysiwyg' => FALSE,                  // disables wysiwig editors
      '#description' => t('Display at bottom of popup. This field may contain &lt;?php ?&gt; PHP code.')
    );
  }
}

/**
 * Theme views popup UI
 */
function theme_views_popup_ui_style_plugin_table($form) {
  $output = drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Show In'),
    t('Column'),
    t('Separator'),
    array(
      'data' => t('Sortable'),
      'align' => 'center',
    ),
    array(
      'data' => t('Default sort'),
      'align' => 'center',
    ),
  );
  $rows = array();
  foreach (element_children($form['columns']) as $id) {
    $row = array();
    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['info'][$id]['popup']);
    $row[] = drupal_render($form['columns'][$id]);
    $row[] = drupal_render($form['info'][$id]['separator']);
    if (!empty($form['info'][$id]['sortable'])) {
      $row[] = array(
        'data' => drupal_render($form['info'][$id]['sortable']),
        'align' => 'center',
      );
      $row[] = array(
        'data' => drupal_render($form['default'][$id]),
        'align' => 'center',
      );
    }
    else {
      $row[] = '';
      $row[] = '';
    }
    $rows[] = $row;
  }

  // Add the special 'None' row.
  $rows[] = array(t('None'), '', '', '', '', array('align' => 'center', 'data' => drupal_render($form['default'][-1])));

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

