
Drupal 5 and 6 version by William Wong - bwong
Rewritten for Drupal 6 by Matthew Parker - matt_p

Views popup
--------------------
OVERVIEW

The views_popup module adds popup style support for a Views List View.
This version is actually listed as "List View as Popup". This module
requires the Views module.


INSTALLING VIEWS

The views_popup module is installed simply by activating the module.


DOCUMENTATION

Create or open a view. Select "Popup List" or "Popup Table" style.
Select one or more fields in for the view. These will be listed in
the style settings with a Show In selection that lets you specify
what fields will be in the normal view and/or the popup.

The popup is implemented using Javascript. This allows it to work with
most popular web browsers. The default implementation has a half
second delay before the first popup is displayed. This allows a user
to quickly move the mouse over the fields and not showing the popups
until the cursor sits atop a field for a little time. There is also
a delay for turning this off since the delay time is set to 0 after
a popup is displayed. This allows the user to scroll down a list of
fields and view each of the popups as necessary.

The delays (in milliseconds) can be edited in the javascript.

The default settings for delays and mouse following are in the
admin section. You need Views administration permission to access
the Views Popup administration page.





