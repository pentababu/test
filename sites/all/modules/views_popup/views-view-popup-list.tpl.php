<?php

/**
 * Template to display a view as a list with popups.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings. Keyed by field ID.
 * - $popup_results : An array of results, each containing:
 *   	* - An id to give to two html elements (the popup element and the table row that instantiates the popup).
 *   	* - Content, two arrays: containing content to display in the table and content to display in the popup.
 * - $popup_headers : Contains two arrays: The headers for the table and the field names for the popup.
 */
?>
<div class="item-list">
  <?php
    if ($title)
      print "<h3>$title></h3>\n";

    if ($popup_id)
      $popup_id = " id='$popup_id'";
    if ($class)
      $class=" class='$class'";
    print "<$list_type$class$popup_id>\n";

    $i = 0;
    $last = count($popup_results);
    foreach ($popup_results as $id => $row) {
      $i = $i + 1;
      $id_num = $row['id_num'];
      $extra = (($i == 1) ? ' first-row' : '') . (($i == $last) ? ' last-row' : '' ) ;

      print "<li rel='#views-popup-$id_num' class='views-popup-row$extra' id='$id_num'>\n".views_popup_theme_row($row,'displayed')."</li>\n";
    }
    print "</$list_type>\n";
  ?>
</div>
<?php
  foreach ($popup_results as $id => $row) {
    $text = '';
    $id = $row['id_num'];
    $text = "<div class='views-popup $popup_class' id='views-popup-$id'>\n";

    if ($popup_header)
      $text .= "<div class='views-popup-header'>$popup_header</div>\n";

    $row_text = views_popup_theme_row($row,'popup');
    if ($row_text) {
      $text .= $row_text;
      if ($popup_footer)
        $text .= "<div class='views-popup-footer'>$popup_footer</div>\n";

      print "$text</div>\n";
    }
  }
?>


