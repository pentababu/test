<?php
module_load_include('inc','views','includes/base');
module_load_include('inc','views','includes/plugins');
module_load_include('inc','views','plugins/views_plugin_style');
module_load_include('inc','views','plugins/views_plugin_style_list');

/**
 * Style plugin to render each item in a grid cell.
 */
class views_popup_plugin_style_views_popup_grid extends views_plugin_style_list {
  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['grid_columns'] = array('default' => '4');
    $options['alignment'] = array('default' => 'horizontal');
    $options['fill_single_line'] = array('default' => TRUE);

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['#theme'] = 'views_popup_ui_style_plugin_grid';

    $handlers = $this->view->display_handler->get_handlers('field');

    $fields = $this->view->display[$form_state['display_id']]->display_options['fields'];
    
    foreach ((is_array($fields) ? $fields : $this->view->display['default']->display_options['fields']) as $field => $field_info) {
      $safe = str_replace(array('][', '_', ' '), '-', $field);
      // the $id of the column for dependency checking.
      $id = 'edit-style-options-columns-' . $safe;

      $form['columns'][$field] = array();

      $field = $field_info['id'];

      $form['info'][$field]['name'] = array(
          '#type' => 'item',
          '#value' => $handlers[$field]->definition['group'].': '.$handlers[$field]->definition['title'].': '.$field_info['label'],
        );

      $form['info'][$field]['popup'] = array(
            '#type' => 'select',
            '#options' => array( 0 => t('Grid'), 1 => t('Popup'), 2 => t('Both'), 3 => t('Hide')),
            '#default_value' => $this->options['info'][$field]['popup'],
            '#process' => array('views_process_dependency'),
            '#dependency' => array($id => array($field)),
        );
    }

    foreach ($this->view->display['default']->display_options['fields'] as $key => $x) {
      $keys[]=$key;
    }

    $form['type']['#type'] = 'hidden' ;
    
    
    $form['grid_columns'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of columns'),
      '#default_value' => $this->options['grid_columns'],
    );
    $form['alignment'] = array(
      '#type' => 'radios',
      '#title' => t('Alignment'),
      '#options' => array('horizontal' => t('Horizontal'), 'vertical' => t('Vertical')),
      '#default_value' => $this->options['alignment'],
      '#description' => t('Horizontal alignment will place items starting in the upper left and moving right. Vertical alignment will place items starting in the upper left and moving down.'),
    );
    
    $form['fill_single_line'] = array(
      '#type' => 'checkbox',
      '#title' => t('Fill up single line'),
      '#description' => t('If you disable this option a grid with only one row will have the amount of items as tds. If you disable it this can cause problems with your css.'),
      '#default_value' => !empty($this->options['fill_single_line']),
    );
  }
}

/**
 * Theme views popup UI
 */
function theme_views_popup_ui_style_plugin_grid($form) {
  $output = drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Show In'),
  );
  $rows = array();
  foreach (element_children($form['columns']) as $id) {
    $row = array();
    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['info'][$id]['popup']);
    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

