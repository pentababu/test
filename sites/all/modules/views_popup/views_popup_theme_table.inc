<?php

require_once(drupal_get_path('module', 'views_popup').'/views_popup_theme.inc');

function template_preprocess_views_view_popup_table(&$vars) {
  $view     = $vars['view'];

  // We need the raw data for this grouping, which is passed in as $vars['rows'].
  // However, the template also needs to use for the rendered fields.  We
  // therefore swap the raw data out to a new variable and reset $vars['rows']
  // so that it can get rebuilt.
  $result   = $vars['rows'];
  $vars['rows'] = array();

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;

  $fields   = &$view->field;
  $columns  = $handler->sanitize_columns($options['columns'], $fields);

  $active   = !empty($handler->active) ? $handler->active : '';
  $order    = !empty($handler->order) ? $handler->order : 'asc';

  $query    = tablesort_get_querystring();
  if ($query) {
    $query = '&' . $query;
  }

  // Fields must be rendered in order as of Views 2.3, so we will pre-render
  // everything.
  $renders = array();
  $keys = array_keys($view->field);
  foreach ($result as $count => $row) {
    foreach ($keys as $id) {
      $renders[$count][$id] = $view->field[$id]->theme($row);
    }
  }

  foreach ($columns as $field => $column) {
    // render the header labels
    if ($field == $column && empty($fields[$field]->options['exclude'])) {
      $label = check_plain(!empty($fields[$field]) ? $fields[$field]->label() : '');

      if (empty($options['info'][$field]['sortable'])) {
        $displayed_header = $label;
      } else {
        // @todo -- make this a setting
        $initial = 'asc';
        if ($active == $field && $order == 'asc') {
          $initial = 'desc';
        }

        $image = theme('tablesort_indicator', $initial);
        $title = t('sort by @s', array('@s' => $label));
        $link_options = array(
          'html' => true,
          'attributes' => array('title' => $title),
          'query' => 'order=' . urlencode($field) . '&sort=' . $initial . $query,
        );
        $displayed_header = l($label . $image, $_GET['q'], $link_options);
      }

      switch ($options['info'][$field]['popup']) {
      case 0:
        $popup_headers['displayed'][$field] = $displayed_header;
        break;

      case 1:
        $popup_headers['popup'][$field] = $label;
        break;

      case 2:
        $popup_headers['displayed'][$field] = $displayed_header;
        $popup_headers['popup'][$field] = $label;
        break;
      }
    }

    // Create a second variable so we can easily find what fields we have and what the
    // CSS classes should be.
    $vars['fields'][$field] = views_css_safe($field);
    if ($active == $field) {
      $vars['fields'][$field] .= ' active';
    }

    // Render each field into its appropriate column.
    foreach ($result as $num => $row) {
      if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
        $field_output = $renders[$num][$field];

        // Don't bother with separators and stuff if the field does not show up.
        if (!isset($field_output) && isset($vars['rows'][$num][$column])) {
          continue;
        }

        // Place the field into the column, along with an optional separator.
        if (isset($vars['rows'][$num][$column])) {
          if (!empty($options['info'][$column]['separator'])) {
            $vars['rows'][$num][$column] .= filter_xss_admin($options['info'][$column]['separator']);
          }
        }
        else {
          $vars['rows'][$num][$column] = '';
        }

        $vars['rows'][$num][$column] .= $field_output;
      }
    }
  }

  $vars['class'] = 'views-table';
  if (!empty($options['sticky'])) {
    drupal_add_js('misc/tableheader.js');
    $vars['class'] .= " sticky-enabled";
  }

  /*********************
   * VIEWS POPUP OUTPUT *
  **********************/

  $results = array();
  foreach($vars['rows'] as $count => $row) {
    $popup_new = array();
    $row_new   = array();
    foreach($row as $field => $content) {
      switch ($options['info'][$field]['popup']) {
      case 0:
        $row_new[$field]   = $content; // The fields to be displayed in the popup
        break;

      case 1:
        $popup_new[$field] = $content; // The fields to be displayed in a table
        break;

      case 2:
        $popup_new[$field] = $content; // The fields to be displayed in the popup
        $row_new[$field]   = $content; // The fields to be displayed in a table
        break;
      }
    }

    $curr_result['id_num'] = views_popup_id_num($view->name);
    $curr_result['content']['displayed'] = $row_new;
    $curr_result['content']['popup']     = $popup_new;
    $results[] = $curr_result;
  }
  $vars['popup_results'] = $results; // Set the variables to be retrieved in the template.
  $vars['popup_headers'] = $popup_headers;

  $vars['popup_header'] = drupal_eval($options['views_popup_header']);
  $vars['popup_footer'] = drupal_eval($options['views_popup_footer']);
  $vars['popup_class']  = $options['views_popup_class'];

  _views_popup_add_js();
}
