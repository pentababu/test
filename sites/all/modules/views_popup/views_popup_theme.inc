<?php

function _views_popup_add_js() {
  static $add_once = true ;
  if ($add_once) {
    $add_once = false ;

    drupal_add_css(drupal_get_path('module', 'views_popup') .'/views_popup.css');
    drupal_add_js(array('views_popup' => array( // pass the module options to the javascript that handles the popups
	  'popup_delay' => variable_get('views_popup_delay', 500),
	  'reset_time' => variable_get('views_popup_reset', 1000),
	  'follow_mouse' => variable_get('views_popup_follow_mouse', false)
    )), 'setting');
    drupal_add_js(drupal_get_path('module', 'views_popup').'/views_popup.js');
  }
}
