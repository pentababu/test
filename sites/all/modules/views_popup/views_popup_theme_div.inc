<?php

require_once(drupal_get_path('module', 'views_popup').'/views_popup_theme.inc');

function template_preprocess_views_view_popup_div(&$vars) {
  $view     = $vars['view'];

  // We need the raw data for this grouping, which is passed in as $vars['rows'].
  // However, the template also needs to use for the rendered fields.  We
  // therefore swap the raw data out to a new variable and reset $vars['rows']
  // so that it can get rebuilt.
  $result   = $vars['rows'];
  $vars['rows'] = array();

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;
  $fields   = &$view->field;

  $query    = tablesort_get_querystring();
  if ($query) {
    $query = '&' . $query;
  }

  $vars['class'] = 'views-list';

  // Fields must be rendered in order as of Views 2.3, so we will pre-render
  // everything.
  $renders = array();
  $keys = array_keys($view->field);
  foreach ($result as $count => $row) {
    foreach ($keys as $id) {
      $renders[$count][$id] = $view->field[$id]->theme($row);
    }
  }


  /*********************
   * VIEWS POPUP OUTPUT *
  **********************/

  $results = array();
  foreach ( $result as $count => $row ) {
    $row_new = array();
    $popup_new = array();
    $label_new = array();

    foreach ( $options['info'] as $field => $option ) {
      if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
        $content = $renders[$count][$field] ;//$fields[$field]->theme($row);

        if ($content) {
          $label_new[$field] = $fields[$field]->label();
          switch ($option['popup']) {
          case 0:
	        $row_new[$field] = $content; // The fields to be displayed in a table
            break;

          case 1:
	        $popup_new[$field] = $content; // The fields to be displayed in the popup
            break;

          case 2:
	        $popup_new[$field] = $content; // The fields to be displayed in the popup
	        $row_new[$field] = $content; // The fields to be displayed in a table
            break;
	      }
        }
      }
    }
    $curr_result['id_num'] = views_popup_id_num($view->name);
    $curr_result['content']['displayed'] = $row_new;
    $curr_result['content']['popup'] = $popup_new;
    $curr_result['content']['label'] = $label_new;
    $results[] = $curr_result;
  }

  $vars['popup_results'] = $results; // Set the variables to be retrieved in the template.

  $vars['popup_header'] = drupal_eval($options['views_popup_header']);
  $vars['popup_footer'] = drupal_eval($options['views_popup_footer']);
  $vars['popup_class']  = $options['views_popup_class'];

  _views_popup_add_js();
}


function views_popup_theme_row_div($row_result,$type){
  $row = array();
  $headers = $row_result['content']['label'];
  foreach ($row_result['content'][$type] as $field => $content) {
    $row[] = "<div class='views-popup-field-$field'>"
        . ( $headers[$field] ? ( "<label class='views-popup-label'>$headers[$field]: </label>" ) : '' )
        . "<span class='views-popup-content'>$content</span></div>\n" ;
  }
  return implode($row,"\n");
}

