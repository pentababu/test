<?php

module_load_include('inc','views','includes/base');
module_load_include('inc','views','includes/plugins');
module_load_include('inc','views','plugins/views_plugin_style');
module_load_include('inc','views','plugins/views_plugin_style_list');

/**
 * Admin settings callback
 */
class views_popup_plugin_style_views_popup_div extends views_plugin_style_list {
  function option_definition() {
    $options = parent::option_definition();

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['#theme'] = 'views_popup_ui_style_plugin_div';

    $handlers = $this->view->display_handler->get_handlers('field');

    $fields = $this->view->display[$form_state['display_id']]->display_options['fields'];
    
    foreach ((is_array($fields) ? $fields : $this->view->display['default']->display_options['fields']) as $field => $field_info) {
      $safe = str_replace(array('][', '_', ' '), '-', $field);
      // the $id of the column for dependency checking.
      $id = 'edit-style-options-columns-' . $safe;

      $form['columns'][$field] = array();

      $field = $field_info['id'];

      $form['info'][$field]['name'] = array(
          '#type' => 'item',
          '#value' => $handlers[$field]->definition['group'].': '.$handlers[$field]->definition['title'].': '.$field_info['label'],
        );

      $form['info'][$field]['popup'] = array(
            '#type' => 'select',
            '#options' => array( 0 => 'List', 1 => 'Popup', 2 => 'Both', 3 => 'Hide' ),
            '#default_value' => $this->options['info'][$field]['popup'],
            '#process' => array('views_process_dependency'),
            '#dependency' => array($id => array($field)),
        );
    }

    foreach ($this->view->display['default']->display_options['fields'] as $key => $x) {
      $keys[]=$key;
    }

    $form['type']['#type'] = 'hidden' ;

    $form['views_popup_class'] = array(
      '#type' => 'textfield',
      '#title' => t('CSS Class'),
      '#default_value' => $this->options['views_popup_class'],
      '#size' => 60,
      '#description' => t('CSS class for popup.')
    );

    $form['views_popup_header'] = array(
      '#type' => 'textarea',
      '#title' => t('Header'),
      '#default_value' => $this->options['views_popup_header'],
      '#rows' => 4,
      '#wysiwyg' => FALSE,                  // disables wysiwig editors
      '#description' => t('Display at top of popup. This field may contain &lt;?php ?&gt; PHP code.')
    );

    $form['views_popup_footer'] = array(
      '#type' => 'textarea',
      '#title' => t('Footer'),
      '#default_value' => $this->options['views_popup_footer'],
      '#rows' => 4,
      '#wysiwyg' => FALSE,                  // disables wysiwig editors
      '#description' => t('Display at bottom of popup. This field may contain &lt;?php ?&gt; PHP code.')
    );
  }
}

/**
 * Theme views popup UI
 */
function theme_views_popup_ui_style_plugin_div($form) {
  $output = drupal_render($form['description_markup']);

  $header = array(
    t('Field'),
    t('Show In'),
  );
  $rows = array();
  foreach (element_children($form['columns']) as $id) {
    $row = array();
    $row[] = drupal_render($form['info'][$id]['name']);
    $row[] = drupal_render($form['info'][$id]['popup']);
    $rows[] = $row;
  }

  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form);
  return $output;
}

