<?php

/**
* Implementation of hook_views_plugin
*
*
*/
function views_popup_views_plugins() {
  return array(
    'module' => 'views_popup', // This just tells our themes are elsewhere.

    'style' => array( //declare the views_popup style plugin
      'views_popup_div' => array(
        'title' => t('Popup Unformatted'),
        'help' => t('Displays an unformatted list with popup fields'),
        'handler' => 'views_popup_plugin_style_views_popup_div',
        'theme' => 'views_view_popup_div',
        'theme file' => 'views_popup_theme_div.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
      'views_popup_list' => array(
        'title' => t('Popup List'),
        'help' => t('Displays list with popup fields'),
        'handler' => 'views_popup_plugin_style_views_popup_list',
        'theme' => 'views_view_popup_list',
        'theme file' => 'views_popup_theme_list.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
      'views_popup_table' => array(
        'title' => t('Popup Table'),
        'help' => t('Displays table with popup fields'),
        'handler' => 'views_popup_plugin_style_views_popup_table',
        'theme' => 'views_view_popup_table',
        'theme file' => 'views_popup_theme_table.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
      'views_popup_grid' => array(
        'title' => t('Popup Grid'),
        'help' => t('Displays grid with popup fields'),
        'handler' => 'views_popup_plugin_style_views_popup_grid',
        'theme' => 'views_view_popup_grid',
        'theme file' => 'views_popup_theme_grid.inc',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}




