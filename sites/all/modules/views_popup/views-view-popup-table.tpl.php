<?php

/**
 * Template to display a view as a table with popups.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $class: A class or classes to apply to the table, based on settings. Keyed by field ID.
 * - $popup_results : An array of results, each containing:
 *   	* - An id to give to two html elements (the popup element and the table row that instantiates the popup).
 *   	* - Content, two arrays: containing content to display in the table and content to display in the popup.
 * - $popup_headers : Contains two arrays: The headers for the table and the field names for the popup.
 */
?>
<table class="<?php print $class; ?>" <?php if ($popup_id) print "id='$popup_id'"; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($popup_headers['displayed'] as $field => $label): ?>
        <th class="views-field views-field-<?php print $fields[$field]; ?>">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $i = 0;
      $last = count($popup_results);
      foreach ($popup_results as $count => $row) {
        $i = $i + 1;
        $extra = (($i == 1) ? ' first-row' : '') 
               . (($i == $last) ? ' last-row' : '' ) 
               . (($count % 2 == 0) ? ' even' : ' odd' ) ;
        $id = $row['id_num'];
        print "<tr rel='#views-popup-$id' id='$id' class='views-popup-row$extra'>\n";
        foreach ($row['content']['displayed'] as $field => $content) {
          print "<td class='views-field views-field-$field$extra'>$content</td>\n";
        }
        print "</tr>\n";
      }
    ?>
  </tbody>
</table>

<?php
  if ($popup_header) {
    $header = "<div class='views-popup-header'>$popup_header</div>\n";
  }
  if ($popup_footer) {
    $footer = "<div class='views-popup-footer'>$popup_footer</div>\n";
  }
  foreach($popup_results as $count => $row) {
    $id=$row['id_num'];
    if ($row['content']['popup']) {
      $row_content = '';
      foreach($row['content']['popup'] as $field => $content) {
        if ($content) {
          $row_content .= "<div class='views-popup-field-$field'>";
          $label = $popup_headers['popup'][$field];
          if ($label){
            $row_content .= "<label class='views-popup-label'>$label:</label> ";
          }
          $row_content .= "<span class='views-popup-content'>$content</span></div>";
        }
      }
    }
    if ($row_content) {
      print "<div class='views-popup $popup_class$extra' id='views-popup-$id'>\n"
           .$header
           .$row_content
           .$footer
           ."</div>\n";
    }
  }

