<?php

require_once(drupal_get_path('module', 'views_popup').'/views_popup_theme.inc');

function template_preprocess_views_view_popup_grid(&$vars) {
  $view     = $vars['view'];

  // We need the raw data for this grouping, which is passed in as $vars['rows'].
  // However, the template also needs to use for the rendered fields.  We
  // therefore swap the raw data out to a new variable and reset $vars['rows']
  // so that it can get rebuilt.
  $result   = $vars['rows'];
  $vars['rows'] = array();

  $options  = $view->style_plugin->options;
  $handler  = $view->style_plugin;
  $fields   = &$view->field;

  $grid_columns     = $options['grid_columns'];
  $fill_single_line = $options['fill_single_line'];
  
  $query    = tablesort_get_querystring();
  if ($query) {
    $query = '&' . $query;
  }

  $vars['class'] = 'views-list';

  // Fields must be rendered in order as of Views 2.3, so we will pre-render
  // everything.
  $renders = array();
  $keys = array_keys($view->field);
  foreach ($result as $count => $row) {
    foreach ($keys as $id) {
      $renders[$count][$id] = $view->field[$id]->theme($row);
    }
  }


  /*********************
   * VIEWS POPUP OUTPUT *
  **********************/

  $results = array();
  foreach ( $result as $count => $row ) {
    $row_new = array();
    $popup_new = array();
    $label_new = array();

    foreach ( $options['info'] as $field => $option ) {
      if (!empty($fields[$field]) && empty($fields[$field]->options['exclude'])) {
        $content = $renders[$count][$field] ;//$fields[$field]->theme($row);

        if ($content) {
          $label_new[$field] = $fields[$field]->label();
          switch ($option['popup']) {
          case 0:
	        $row_new[$field] = $content; // The fields to be displayed in a table
            break;

          case 1:
	        $popup_new[$field] = $content; // The fields to be displayed in the popup
            break;

          case 2:
	        $popup_new[$field] = $content; // The fields to be displayed in the popup
	        $row_new[$field] = $content; // The fields to be displayed in a table
            break;
	      }
	    }
      }
    }
    $curr_result['id_num'] = views_popup_id_num($view->name);
    $curr_result['content']['displayed'] = $row_new;
    $curr_result['content']['popup'] = $popup_new;
    $curr_result['content']['label'] = $label_new;
    $results[] = $curr_result;
  }

  // rearrange into grid, from Views theme.inc function template_preprocess_views_view_grid

  $popup_grid_empty = -1;
  
  if ($options['alignment'] == 'horizontal') {
    $row = array();
    $row_count = 0;
    foreach ($results as $count => $item) {
      $row[] = $count;
      $row_count++;
      if (($count + 1) % $grid_columns == 0) {
        $rows[] = $row;
        $row = array();
        $row_count = 0;
      }
    }
    if ($row) {
      // Fill up the last line only if it's configured, but this is default.
      if (!empty($fill_single_line) || count($rows)) {
        for ($i = 0; $i < ($grid_columns - $row_count); $i++) {
          $row[] = $popup_grid_empty;
        }
      }
      $rows[] = $row;
    }
  }
  else {
    $num_rows = floor(count($results) / $grid_columns);
    // The remainders are the 'odd' columns that are slightly longer.
    $remainders = count($results) % $grid_columns;
    $row = 0;
    $col = 0;
    foreach ($results as $count => $item) {
      $rows[$row][$col] = $count;
      $row++;

      if (!$remainders && $row == $num_rows) {
        $row = 0;
        $col++;
      }
      else if ($remainders && $row == $num_rows + 1) {
        $row = 0;
        $col++;
        $remainders--;
      }
    }
    for ($i = 0; $i < count($rows[0]); $i++) {
      // This should be string so that's okay :)
      if (!isset($rows[count($rows) - 1][$i])) {
        $rows[count($rows) - 1][$i] = $popup_grid_empty;
      }
    }
  }
  
  // Set the variables to be retrieved in the template.
  
  $vars['popup_results']    = $results;
  $vars['popup_grid']       = $rows;
  $vars['popup_grid_empty'] = $popup_grid_empty;
  
  $vars['popup_header'] = drupal_eval($options['views_popup_header']);
  $vars['popup_footer'] = drupal_eval($options['views_popup_footer']);
  $vars['popup_class']  = $options['views_popup_class'];

  _views_popup_add_js();
}


function views_popup_theme_row_grid($row_result,$type){
  $row = array();
  $headers = $row_result['content']['label'];
  $fields=$row_result['content'][$type];
  if(is_array($fields)){
    foreach ($fields as $field => $content) {
      $row[] = "<div class='views-popup-field-$field'>"
          . ( $headers[$field] ? ( "<label class='views-popup-label'>$headers[$field]: </label>" ) : '' )
          . "<span class='views-popup-content'>$content</span></div>\n" ;
    }
  }
  return implode($row,"\n");
}

