<style>
td img{padding:2px;border:1px solid #ccc;background:#fefefe;}
tr.group-desc p{margin:0;}
</style>
<?php print $node->title;?>
<span style="background:#fcfcfc;-webkit-box-shadow:0px 0px 1px 1px #ccc;display:block;padding:5px;padding-top:10px;">
<table><tbody style="border:none;"><tr class="group-desc"><td style="vertical-align:top;"><?php print $node->field_group_image[0]['view'] ;?></td>
<td style="padding-left:10px;vertical-align:top;"><?php print $node->content['og_mission']['#value'];?></td></tr></tbody></table>
<table><tbody style="border:none;">
<tr><td style="text-align:right;padding-right:0px;width:155px;">Website:</td>
<td style="padding-left:16px;"><?php print $node->field_group_link[0]['view'] ;?><br/></td></tr>
<tr><td style="text-align:right;padding-right:0px;width:155px;">Location:</td>
<td style="padding-left:16px;"><?php print $node->field_group_location[0]['view'];?></td></tr>
<tr><td style="text-align:right;padding-right:0px;width:155px;">Language:</td>
<td style="padding-left:16px;"><?php print $node->field_language[0]['view'] ?></td></tr>
<tr><td style="text-align:right;padding-right:0px;width:155px;">Thematic Areas:</td>
<td style="padding-left:16px;"><?php print $terms;?></td></tr>
<tr><td style="text-align:right;padding-right:0px;width:155px;">Created on:</td>
<td style="padding-left:16px;"><?php echo date("D, j M Y \a\\t G:i", $created) ?></td></tr>
<tr><td></td></tr></tbody></table></span>

