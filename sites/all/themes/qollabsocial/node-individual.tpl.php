<style>
#profile-left {
display:inline-block;
width:15%;
vertical-align:top;
font-weight:bold;
font-size:12px;
text-transform:capitalize;
}
h1{display:none;}
.picture img{border:5px solid #eef8fe;}
#content-inner-inner {
    border-left:none;
    border-right:none;
}
.view-user-blog-aggregate {
  margin-bottom:2px;
}
.view-user-blog-aggregate .views-field-title {
  font-weight:bold;
}
.view-user-blog-aggregate .row-1 .col-2 .views-field-name{
  border-bottom:2px solid #ddd;
  width:90%;
  padding-bottom:8px;
}
.view-user-blog-aggregate .row-1 .col-1 .views-field-name{
 border-bottom:2px solid #ddd;
  width:90%;
  padding-bottom:8px;
}
.view-user-blog-aggregate .row-2 .col-2 {
  margin-left:20px;
}
ul.pager li.pager-current {
    background-color: #777777;
    color: #FFFFFF;
    display: none;
}
.item-list .pager li.pager-previous {
    background-image: url("/sites/all/themes/qollabsocial/images/profile-scroller.png");
    background-repeat: no-repeat;
    display: none;
    height: 23px;
    list-style-type: none;
    padding: 0;
    width: 22px;
}
.item-list .pager {
    clear: both;
    text-align: right;
}
.item-list .pager li.pager-next {
    background-image: url(/sites/all/themes/qollabsocial/images/profile-scroller.png);
background-repeat: no-repeat;
    display: inline-block;
    list-style-type: none;
    padding: 0;
    width:16px;
    height:21px;
}
ul.pager a, ul.pager li.pager-current {
    border-color: #333333;
}
ul.pager a, ul.pager li.pager-current {
    border:none;
    padding:0px;
    text-decoration: none;
}
.imagecache-thumbnail{
 -moz-box-shadow:1px 1px 2px #000000;
 -webkit-box-shadow:1px 1px 2px #000000;
}
.view-user-related-videos .views-field-name a {
  font-weight:bold;
}
.view-id-user_albums h2 {
    color: #333333;
    font-size: 12px;
    font-weight: bold;
    margin-left: 9px;
    margin-top: 130px;
    padding-top: 5px;
    position: absolute;
    text-shadow: 1px 1px 1px #eee;
}
.views-field-field-picture-fid {
/*background-image: url("/sites/all/themes/qollabsocial/images/homepostwrap.png");*/
/*    background:#222932;*/
/*    background:#efefef;*/
    -moz-box-shadow: 0.8px 0px 2px 2px#ccc;
    -webkit-box-shadow: 0.8px 0px 2px 1px #ccc;
    float: left;
    height: 163px;
    margin-bottom: 22px;
    margin-right: 30px;
    padding: 7px 2px;
    width: 174px;
    z-index: 1;
}
</style>
<!--<div id="profile-left"><?php $my_node_type = 'individual';
if ($node->type == $my_node_type && arg(0) == 'node' && !empty($node->picture)) {
  $picture_url = base_path() . $node->picture;
  $picture_alt = 'User\'s picture';
  print '<span class="picture">'. "\n" .'<img src="'. $picture_url .'" alt="'. $picture_alt .'" title="'. $picture_alt .'" />'. "\n" .'</span>';
}?><?php print $node->field_first_name[0]['view'] ?>&nbsp;<?php print $node->field_last_name[0]['view'] ?></div>-->
<!-- Tabs for the profile of an individual-->
<!--javascript-->
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
      case "tab4":
	res.innerHTML=document.getElementById("tab4Content").innerHTML;
	break;
      case "tab5":
	res.innerHTML=document.getElementById("tab5Content").innerHTML;
	break;
      case "tab6":
	res.innerHTML=document.getElementById("tab6Content").innerHTML;
	break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>
<!--end of javascript-->
<!--style for the profile pages-->
<style>
  .tabContainer{margin:10px 0;width:86.3%;display:inline-block;/*-moz-box-shadow: 0px 0px 8px #c4c4c4;-webkit-box-shadow: 0px 0px 8px #c4c4c4;box-shadow: 0px 0px 8px #c4c4c4;*/ border-left:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed;
}
  .tabContainer .digiTabs{list-style:none;display:block;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{text-align:center;width:99px;float:left;padding:5px!important;cursor:pointer;border-bottom:none;font-family:Lucida Grande, "Verdana";font-size:14px;font-weight:bold;color:#393939;-moz-box-shadow: 0px 0px 3px #c4c4c4;-webkit-box-shadow: 0px 0px 3px #c4c4c4;box-shadow: 0px 0px 3px #c4c4c4;margin-right:0px;}
  .tabContainer .digiTabs .selected{background-color:#fff;color:#393939;-moz-box-shadow:0px -2px 3px #C4C4C4;-webkit-box-shadow:0px -2px 3px #C4C4C4;box-shadow:0px -2px 3px #C4C4C4;}
  #tabContent{padding:25px 10px 10px 20px;background-color:#fff;float:left;margin-bottom:10px;/*border:1px solid #e1e1e1;*/width:93%;}
  .profile-bio-content { background:#f8f9f9;margin-top:5px;margin-bottom:5px;padding:5px;}
  #profile-fields-label{/*font-weight:bold;*/font-size:12px;display:inline-block;width:100px;vertical-align:top;}
  .profile-field-item{display:inline-block;font-weight:normal;width:175px;color:#333333;}
  #profile-fields-label-social{/*font-weight:bold;*/font-size:12px;display:inline-block;width:260px;vertical-align:top;margin-bottom:10px;}
  .profile-field-item-social{display:inline-block;font-weight:normal;width:335px;color:#333333;margin-bottom:10px;}
  .profile-fields {display:inline-block;width:auto;float:left;}
  .profile-fields-right {display:inline-block;width:auto; float:right;}
#content-group-inner{
-moz-box-shadow: none;-webkit-box-shadow: none;box-shadow: none;
margin:0;overflow:visible;
min-height:500px;
}
</style>
<!--end of styles-->
<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">General</li>
    <li id="tab2" onclick="tabs(this);">Professional</li>
    <li id="tab3"  onclick="tabs(this);">Social</li>
    <li id="tab4"  onclick="tabs(this);">Multimedia</li>
    <li id="tab5"  onclick="tabs(this);">Events</li>
    <li id="tab5"  onclick="tabs(this);">Projects</li>
  </ul>
  <div id="tabContent">

<div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">About Me</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;padding-bottom:5px;">Basic Information</div>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">First Name:</div>
      <div class="profile-field-item"><?php print $node->field_first_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Date of Birth:</div>
      <div class="profile-field-item"><?php print $node->field_dob[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Last Name:</div>
      <div class="profile-field-item"><?php print $node->field_last_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Languages:</div>
      <div class="profile-field-item"><?php print $node->field_languages[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sex:</div>
      <div class="profile-field-item"><?php print $node->field_gender[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Martial Status:</div>
      <div class="profile-field-item"><?php print $node->field_martial_status[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Here for:</div>
      <div class="profile-field-item"><?php print $node->field_here_for[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Contact No:</div>
      <div class="profile-field-item"><?php print $node->field_contact_number_p[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<div style="color:#0aa4de;font-size:12.5px;font-weight:bold;padding-top:15px;padding-bottom:5px;">Personal</div>
<table style="width:47%;display:inline-block;color:#0aa4de;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Interests:</div>
      <div class="profile-field-item"><?php print $node->field_interests[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Industry:</div>
      <div class="profile-field-item"><?php print $node->field_industry[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Activities:</div>
      <div class="profile-field-item"><?php print $node->field_activities[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Political View:</div>
      <div class="profile-field-item"><?php print $node->field_political_view[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">5 things I can live without:</div>
      <div class="profile-field-item"><?php print $node->field_five_things[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sports:</div>
      <div class="profile-field-item"><?php print $node->field_sports[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Music:</div>
      <div class="profile-field-item"><?php print $node->field_music[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;color:#0aa4de;">
<table><tbody style="border:none;color:#0aa4de;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_indi_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<center><a href="/messages/new/<?php print $uid;?>"><img src="/sites/all/themes/qollabsocial/images/sendmessage.jpg"/></a></center>
</td></tr>
</tbody></table>
</div>
</div>
</div>
 
<div id="tab1Content" style="display:none;">
<div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">About Me:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;padding-bottom:5px;">Basic Information:</div>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">First Name:</div>
      <div class="profile-field-item"><?php print $node->field_first_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Date of Birth:</div>
      <div class="profile-field-item"><?php print $node->field_dob[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Last Name:</div>
      <div class="profile-field-item"><?php print $node->field_last_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Languages:</div>
      <div class="profile-field-item"><?php print $node->field_languages[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sex:</div>
      <div class="profile-field-item"><?php print $node->field_gender[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Martial Status:</div>
      <div class="profile-field-item"><?php print $node->field_martial_status[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Here for:</div>
      <div class="profile-field-item"><?php print $node->field_here_for[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Contact No:</div>
      <div class="profile-field-item"><?php print $node->field_contact_number_p[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<div style="color:#0aa4de;font-size:12.5px;font-weight:bold;padding-top:15px;padding-bottom:5px;">Personal</div>
<table style="width:47%;display:inline-block;color:#0aa4de;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Interests:</div>
      <div class="profile-field-item"><?php print $node->field_interests[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Industry:</div>
      <div class="profile-field-item"><?php print $node->field_industry[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Activities:</div>
      <div class="profile-field-item"><?php print $node->field_activities[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Political View:</div>
      <div class="profile-field-item"><?php print $node->field_political_view[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">5 things I can live without:</div>
      <div class="profile-field-item"><?php print $node->field_five_things[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sports:</div>
      <div class="profile-field-item"><?php print $node->field_sports[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Music:</div>
      <div class="profile-field-item"><?php print $node->field_music[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;">
<table><tbody style="border:none;color:#0aa4de;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_indi_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<center><a href="/messages/new/<?php print $uid;?>"><img src="/sites/all/themes/qollabsocial/images/sendmessage.jpg"/></a></center>
</td></tr>
</tbody></table>
</div>
</div></div>
<div id="tab2Content" style="display:none;"><div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">Headline:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_headline[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;">Professional Information:</div>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Job Title:</div>
      <div class="profile-field-item"><?php print $node->field_job[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Job Description:</div>
      <div class="profile-field-item"><?php print $node->field_job_description[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Occupation</div>
      <div class="profile-field-item"><?php print $node->field_occupation[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Company:</div>
      <div class="profile-field-item"><?php print $node->field_company[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Company Webpage:</div>
      <div class="profile-field-item"><?php print $node->field_company_link[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Education:</div>
      <div class="profile-field-item"><?php print $node->field_education[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#ef53b3;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">High School:</div>
      <div class="profile-field-item"><?php print $node->field_high_school[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Year</div>
      <div class="profile-field-item"><?php print $node->field_school_year[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<div style="color:#0aa4de;font-size:12.5px;font-weight:bold;padding-top:15px;padding-bottom:5px;">Personal</div>
<table style="width:47%;display:inline-block;color:#0aa4de;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Interests:</div>
      <div class="profile-field-item"><?php print $node->field_interests[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Industry:</div>
      <div class="profile-field-item"><?php print $node->field_industry[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Activities:</div>
      <div class="profile-field-item"><?php print $node->field_activities[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Political View:</div>
      <div class="profile-field-item"><?php print $node->field_political_view[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">5 things I can live without:</div>
      <div class="profile-field-item"><?php print $node->field_five_things[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sports:</div>
      <div class="profile-field-item"><?php print $node->field_sports[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Music:</div>
      <div class="profile-field-item"><?php print $node->field_music[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;">
<table><tbody style="border:none;color:#0aa4de;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_indi_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<center><a href="/messages/new/<?php print $uid;?>"><img src="/sites/all/themes/qollabsocial/images/sendmessage.jpg"/></a></center>
</td></tr></tbody></table>
</div>
</div>
</div>
<div id="tab3Content" style="display:none;">
<span style="color:#0aa4de;">
<div class="profile-fields">
  <div id="profile-fields-label-social">Social problems you would like solved:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_prob[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Where do you think you will fit in?:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_prob_role[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Previous/Present social work experience:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_work_exp[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Work you like:</div>
      <div class="profile-field-item-social"><?php print $node->field_work_you_like[0]['view'] ?></div>
</div></span></div>
<!-- Start of Multimedia Tab-->
<div id="tab4Content" style="display:none;">
<?php 
$view = views_get_view('user_blog_aggregate');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>
<?php
$view = views_get_view('user_related_videos');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>

<?php
$view = views_get_view('user_albums');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>
</div>
<!-- End of Multimedia Tab-->

<!--Start of Events Tab-->
<div id="tab5Content" style="display:none;">
<?php
$view = views_get_view('user_events_list');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>

</div>
<!-- End of Events Tab -->

<!--Start of Projects Tab-->
<div id="tab6Content" style="display:none;">
</div>
<!--End of Projects Tab-->

<!--end of the tabs-->

