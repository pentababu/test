<style>
#profile-left {
display:inline-block;
width:15%;
vertical-align:top;
font-weight:bold;
font-size:12px;
text-transform:capitalize;
}
.profile-field-item-wide {
display: inline-block;
font-weight: normal;
width: 495px;
color: #333;
}
.profile-bio-content-no-bg {
background: #FFFFFF;
margin-top: 5px;
margin-bottom: 5px;
padding: 5px;display:inline-block;
}
h1{display:none;}
.picture img{border:5px solid #eef8fe;}
#content-inner-inner {
    border-left:none;
    border-right:none;
}
.view-user-blog-aggregate {
  margin-bottom:2px;
}
.view-user-blog-aggregate .views-field-title {
  font-weight:bold;
}
.view-user-blog-aggregate .row-1 .col-2 .views-field-name{
  border-bottom:2px solid #ddd;
  width:90%;
  padding-bottom:8px;
}
.view-user-blog-aggregate .row-1 .col-1 .views-field-name{
 border-bottom:2px solid #ddd;
  width:90%;
  padding-bottom:8px;
}
.view-user-blog-aggregate .row-2 .col-2 {
  margin-left:20px;
}
ul.pager li.pager-current {
    background-color: #777777;
    color: #FFFFFF;
    display: none;
}
.item-list .pager li.pager-previous {
    background-image: url("/sites/all/themes/qollabsocial/images/profile-scroller.png");
    background-repeat: no-repeat;
    display: none;
    height: 23px;
    list-style-type: none;
    padding: 0;
    width: 22px;
}
.item-list .pager {
    clear: both;
    text-align: right;
}
.item-list .pager li.pager-next {
    background-image: url(/sites/all/themes/qollabsocial/images/profile-scroller.png);
background-repeat: no-repeat;
    display: inline-block;
    list-style-type: none;
    padding: 0;
    width:16px;
    height:21px;
}
ul.pager a, ul.pager li.pager-current {
    border-color: #333333;
}
ul.pager a, ul.pager li.pager-current {
    border:none;
    padding:0px;
    text-decoration: none;
}
.imagecache-thumbnail{
 -moz-box-shadow:1px 1px 2px #000000;
 -webkit-box-shadow:1px 1px 2px #000000;
}
.view-user-related-videos .views-field-name a {
  font-weight:bold;
}
.view-id-user_albums h2 {
    color: #333333;
    font-size: 12px;
    font-weight: bold;
    margin-left: 9px;
    margin-top: 130px;
    padding-top: 5px;
    position: absolute;
    text-shadow: 1px 1px 1px #eee;
}
.views-field-field-picture-fid {
/*background-image: url("/sites/all/themes/qollabsocial/images/homepostwrap.png");*/
/*    background:#222932;*/
/*    background:#efefef;*/
    -moz-box-shadow: 0.8px 0px 2px 2px#ccc;
    -webkit-box-shadow: 0.8px 0px 2px 1px #ccc;
    float: left;
    height: 163px;
    margin-bottom: 22px;
    margin-right: 30px;
    padding: 7px 2px;
    width: 174px;
    z-index: 1;
}
td{vertical-align:top;}
</style>
<!--
<script type="text/javascript">
$(document).ready(function() {
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});</script>
<style>
a.morelink {
    text-decoration:none;
    outline: none;
}
.morecontent span {
    display: none;
}
.desc{
width: 400px;
background-color: #F0F0F0;
margin: 10px;}
</style>-->

<!--<div id="profile-left"><?php $my_node_type = 'individual';
if ($node->type == $my_node_type && arg(0) == 'node' && !empty($node->picture)) {
  $picture_url = base_path() . $node->picture;
  $picture_alt = 'User\'s picture';
  print '<span class="picture">'. "\n" .'<img src="'. $picture_url .'" alt="'. $picture_alt .'" title="'. $picture_alt .'" />'. "\n" .'</span>';
}?><?php print $node->field_first_name[0]['view'] ?>&nbsp;<?php print $node->field_last_name[0]['view'] ?></div>-->
<!-- Tabs for the profile of an individual-->
<!--javascript-->
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
      case "tab4":
	res.innerHTML=document.getElementById("tab4Content").innerHTML;
	break;
      case "tab5":
	res.innerHTML=document.getElementById("tab5Content").innerHTML;
	break;
      case "tab6":
	res.innerHTML=document.getElementById("tab6Content").innerHTML;
	break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>
<!--end of javascript-->
<!--style for the profile pages-->
<style>
  .tabContainer{margin:10px 0;width:86.3%;display:inline-block;/*-moz-box-shadow: 0px 0px 8px #c4c4c4;-webkit-box-shadow: 0px 0px 8px #c4c4c4;box-shadow: 0px 0px 8px #c4c4c4;*/ border-left:1px solid #ededed;border-right:1px solid #ededed;border-bottom:1px solid #ededed;
}
  .tabContainer .digiTabs{list-style:none;display:block;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{text-align:center;width:99px;float:left;padding:5px!important;cursor:pointer;border-bottom:none;font-family:Lucida Grande, "Verdana";font-size:14px;font-weight:bold;color:#393939;-moz-box-shadow: 0px 0px 3px #c4c4c4;-webkit-box-shadow: 0px 0px 3px #c4c4c4;box-shadow: 0px 0px 3px #c4c4c4;margin-right:0px;}
  .tabContainer .digiTabs .selected{background-color:#fff;color:#393939;-moz-box-shadow:0px -2px 3px #C4C4C4;-webkit-box-shadow:0px -2px 3px #C4C4C4;box-shadow:0px -2px 3px #C4C4C4;}
  #tabContent{padding:25px 10px 10px 20px;background-color:#fff;float:left;margin-bottom:10px;/*border:1px solid #e1e1e1;*/width:93%;}
  .profile-bio-content { /*background:#f8f9f9;*/margin-top:5px;margin-bottom:5px;padding:5px;}
  #profile-fields-label{/*font-weight:bold;*/font-size:12px;display:inline-block;width:100px;vertical-align:top;}
  .profile-field-item{display:inline-block;font-weight:normal;width:175px;color:#333333;}
  #profile-fields-label-social{/*font-weight:bold;*/font-size:12px;display:inline-block;width:260px;vertical-align:top;margin-bottom:10px;}
  .profile-field-item-social{display:inline-block;font-weight:normal;width:335px;color:#333333;margin-bottom:10px;}
  .profile-fields {display:inline-block;width:auto;float:left;}
  .profile-fields-right {display:inline-block;width:auto; float:right;}
#content-group-inner{
-moz-box-shadow: none;-webkit-box-shadow: none;box-shadow: none;
margin:0;overflow:visible;
min-height:500px;
}
</style>
<!--end of styles-->
<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">Summary</li>
    <li id="tab2" onclick="tabs(this);">NGO Details</li>
    <li id="tab3"  onclick="tabs(this);">Portfolio</li>
    <li id="tab4"  onclick="tabs(this);">Multimedia</li>
    <li id="tab5"  onclick="tabs(this);">Events</li>
    <li id="tab5"  onclick="tabs(this);">Projects</li>
  </ul>
<!-- Start of Tab 1 first display-->
  <div id="tabContent">
<div style="color:#ef53b3;font-size:13px;font-weight:bold;padding-bottom:5px;">Basic Information</div>
<div class="profile-fields">
  <div id="profile-fields-label"><span style="color:#ef53b3;">Organization:</span></div>
      <div class="profile-field-item"><b><?php print $node->field_name[0]['view'] ?></b></div>
</div><br/><br/>
<div class="profile-bio">
  <h3 class="field-label" style="color:#0AA4De;font-size:12px;font-weight:normal;">Mission Statement:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>

<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">NGO Heads:</div>
      <div class="profile-field-item"><?php print $node->field_ngo_heads[0]['view'].', '.$node->field_ngo_heads[1]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Size of Organization:</div>
      <div class="profile-field-item"><?php print $node->field_size_of_the_organization[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branches:</div>
      <div class="profile-field-item"><?php print $node->field_number_of_branches[0]['view'] ?></div>
</div>
</td>
<td>
</td></tr></tbody></table>

<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;padding-top:15px;padding-bottom:5px;">What we do?</div>
<table style="width:47%;display:inline-block;color:#0aa4de;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Thematic Areas:</div>
      <div class="profile-field-item"><?php print $node->field_thematic_area[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Regions Of Activity:</div>
      <div class="profile-field-item"><?php print $node->field_regions_of_activity[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;color:#0aa4de;">
<table><tbody style="border:none;color:#0aa4de;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_contact_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<center><a href="/messages/new/<?php print $uid;?>"><img src="/sites/all/themes/qollabsocial/images/sendmessage.jpg"/></a></center>
</td></tr>
</tbody></table>
</div>

<table style="color:#0aa4de;"><tbody style="border:none;"><tr style="width:100%;"><td>
<div class="profile-fields">
  <div id="profile-fields-label">Awards & Honors:</div>
      <div class="profile-field-item-wide"><?php print $node->field_honors_awards[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Address:</div>
      <div class="profile-field-item-wide"><?php print $node->field_address[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Contact Number:</div>
      <div class="profile-field-item-wide"><?php print $node->field_contact_number[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expertise:</div>
      <div class="profile-field-item-wide"><?php print $node->field_expertise[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Demonstration of Expertise:</div>
      <div class="profile-field-item-wide"><?php print $node->field_demonstration_of_expertise[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
</div>
</div>
<!-- End of Tab 1 First display-->
<!-- Begin Tab 1 Second display --> 
<div id="tab1Content" style="display:none;">
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;padding-bottom:5px;">Basic Information</div>
<div class="profile-fields">
  <div id="profile-fields-label"><span style="color:#0AA4DE;">Organization:</span></div>
      <div class="profile-field-item"><?php print $node->field_name[0]['view'] ?></div>
</div><br/><br/>
<div class="profile-bio">
  <h3 class="field-label" style="color:#0AA4De;font-size:12px;font-weight:normal;">Mission Statement:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>

<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">NGO Heads:</div>
      <div class="profile-field-item"><?php print $node->field_ngo_heads[0]['view'].', '.$node->field_ngo_heads[1]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Size of Organization:</div>
      <div class="profile-field-item"><?php print $node->field_size_of_the_organization[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branches:</div>
      <div class="profile-field-item"><?php print $node->field_number_of_branches[0]['view'] ?></div>
</div>
</td>
<td>
</td></tr></tbody></table>

<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;padding-top:15px;padding-bottom:5px;">What we do?</div>
<table style="width:47%;display:inline-block;color:#0aa4de;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Thematic Areas:</div>
      <div class="profile-field-item"><?php print $node->field_thematic_area[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Regions Of Activity:</div>
      <div class="profile-field-item"><?php print $node->field_regions_of_activity[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;color:#0aa4de;">
<table><tbody style="border:none;color:#0aa4de;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_contact_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<center><a href="/messages/new/<?php print $uid;?>"><img src="/sites/all/themes/qollabsocial/images/sendmessage.jpg"/></a></center>
</td></tr>
</tbody></table>
</div>

<table style="color:#0aa4de;"><tbody style="border:none;"><tr style="width:100%;"><td>
<div class="profile-fields">
  <div id="profile-fields-label">Awards & Honors:</div>
      <div class="profile-field-item-wide"><?php print $node->field_honors_awards[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Address:</div>
      <div class="profile-field-item-wide"><?php print $node->field_address[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Contact Number:</div>
      <div class="profile-field-item-wide"><?php print $node->field_contact_number[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expertise:</div>
      <div class="profile-field-item-wide"><?php print $node->field_expertise[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Demonstration of Expertise:</div>
      <div class="profile-field-item-wide"><?php print $node->field_demonstration_of_expertise[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
</div>
</div>
<!-- End of Tab 1 Second display -->
<div id="tab2Content" style="display:none;"><div class="profile-bio">
<!--  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">Number of branches:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_number_of_branches[0]['view'] ?></div>
  </div>
</div>-->
<div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">Organization History</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_organization_history[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;">Branch Information:</div>
<?php if($node->field_branch_name[0]['view']){?>
<span style="width:290px;display:inline-block;"><table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch name:</div>
      <div class="profile-field-item"><?php print $node->field_branch_name[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch Head:</div>
      <div class="profile-field-item"><?php print $node->field_branch_head[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expertise</div>
      <div class="profile-field-item"><?php print $node->field_branch_expertise[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch Location:</div>
      <div class="profile-field-item"><?php print $node->field_branch_location[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Contact Information:</div>
      <div class="profile-field-item"><?php print $node->field_contact_information[0]['view'] ?></div>
</div>
</td></tr></tbody></table></span>
<div id="branch-map" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;color:#0aa4de;width:260px;height:230px;">
<?php print $node->field_org_location[0]['view'] ?>
<?php print $node->content['fieldgroup_tabs']['group_summary']['field_org_location']['field']['items']['#value']?>
<?php print $node->field_org_location[0]['openlayers_wkt'] ?>
</div><?php }?>
<?php if($node->field_number_of_branches >1){?>
<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>
<span style="width:290px;display:inline-block;"><table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch name:</div>
      <div class="profile-field-item"><?php print $node->field_branch_name[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch Head:</div>
      <div class="profile-field-item"><?php print $node->field_branch_head[1]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expertise</div>
      <div class="profile-field-item"><?php print $node->field_branch_expertise[1]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch Location:</div>
      <div class="profile-field-item"><?php print $node->field_branch_location[1]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table style="width: 290px;"><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Contact Information:</div>
      <div class="profile-field-item"><?php print $node->field_contact_information[1]['view'] ?></div>
</div>
</td></tr></tbody></table></span>
<div id="branch-map" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;color:#0aa4de;width:260px;height:230px;">
<?php print $node->field_org_location[1]['view'] ?>
<?php print $node->content['fieldgroup_tabs']['group_summary']['field_org_location']['field']['items']['#value']?>
<?php print $node->field_org_location[1]['openlayers_wkt'] ?>
</div><?php }?>

</div>
<div id="tab3Content" style="display:none;">
<!--<span style="color:#0aa4de;">
<div class="profile-fields">
  <div id="profile-fields-label-social">Social problems you would like solved:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_prob[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Where do you think you will fit in?:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_prob_role[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Previous/Present social work experience:</div>
      <div class="profile-field-item-social"><?php print $node->field_s_work_exp[0]['view'] ?></div>
</div>

<div class="profile-fields">
  <div id="profile-fields-label-social">Work you like:</div>
      <div class="profile-field-item-social"><?php print $node->field_work_you_like[0]['view'] ?></div>
</div></span>
<div style="border-bottom:1px solid #ccc;width:600px;padding-top:10px;"></div>-->
<!-- Start of Ongoing Projects-->
<?php if($node->field_title[0]['view']){?>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;">Ongoing Projects:</div>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Project name:</div>
      <div class="profile-field-item"><?php print $node->field_title[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Thematic Areas:</div>
      <div class="profile-field-item"><?php print $node->field_project_thematic_area[1]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expected Outcome:</div>
      <div class="profile-field-item"><?php print $node->field_expected_outcome[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Start Date:</div>
      <div class="profile-field-item"><?php print $node->field_project_thematic_area[1]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">End Date:</div>
      <div class="profile-field-item"><?php print $node->field_project_thematic_area[1]['view'] ?></div>
</div></td>
</tr></tbody></table>

<h3 class="field-label" style="color:#0AA4DE;font-size:12px;">Description:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_project_description[0]['view'] ?></div>
  </div>
</div>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expertise</div>
      <div class="profile-field-item"><?php print $node->field_branch_expertise[1]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Branch Head:</div>
      <div class="profile-field-item"><?php print $node->field_branch_head[1]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Branch Location:</div>
      <div class="profile-field-item"><?php print $node->field_branch_location[1]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Contact Information:</div>
      <div class="profile-field-item"><?php print $node->field_contact_information[1]['view'] ?></div>
</div>
</td></tr></tbody></table><?php }?>
<!-- End of Completed Projects-->
<!--Start of Ongoing Projects-->
<?php if($node->field_op_title[0]['view']){?>
<div style="color:#ef53b3;font-size:12.5px;font-weight:bold;">Completed Projects:</div>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Project name:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_title[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Expected Outcome:</div>
      <div class="profile-field-item-wide"><?php print $node->field_expected_outcome[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Start Date:</div>
      <div class="profile-field-item"><?php print $node->field_op_start_date[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">End Date:</div>
      <div class="profile-field-item"><?php print $node->field_op_end_date[0]['view'] ?></div>
</div>
</td>
</tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Thematic Areas:</div>
      <div class="profile-field-item"><?php print $node->field_op_thematic_area[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Regions of Activity:</div>
      <div class="profile-field-item"><?php print $node->field_op_regions_of_activity[0]['view'] ?></div>
</div>
</td>
</tr></tbody></table>
<h3 class="field-label" style="color:#0AA4DE;font-size:12px;">Description:</h3>
  <div class="profile-bio-content">
      <div class="field-item">
<?php
$string = $node->field_op_description[0]['view'];
//$string = substr($string,0,1058);
?>
<?php print $string ?>
</div></div>

<span style="display:block;height:10px;width:100%;"></span>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Indicators Used:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_indicators_used[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Achievements:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_achievements[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Issues Faced:</div>
      <div class="profile-field-item-wide"><?php print $node->field_issues_facing[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Remarks:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_remarks[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Related Files:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_related_files[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;color:#0AA4DE;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">External Links:</div>
      <div class="profile-field-item-wide"><?php print $node->field_op_external_links[0]['view'] ?></div>
</div>
</td></tr></tbody></table>

</span>
<?php }?>
<!--End of Ongoing projects-->
</div>
<!-- Start of Multimedia Tab-->
<div id="tab4Content" style="display:none;">
<?php 
$view = views_get_view('user_blog_aggregate');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>
<?php
$view = views_get_view('user_related_videos');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>

<?php
$view = views_get_view('user_albums');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>
</div>
<!-- End of Multimedia Tab-->

<!--Start of Events Tab-->
<div id="tab5Content" style="display:none;">
<?php
$view = views_get_view('user_events_list');
$view->set_display('block_1');
$output = $view->preview();
$title = $view->get_title();
print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">' . $title . '</span>';?>
<div style="background:#c4c4c4;height:3px;width:100%;margin:5px 0px;"></div>
<?php print ($output);
?>

</div>
<!-- End of Events Tab -->

<!--Start of Projects Tab-->
<div id="tab6Content" style="display:none;">
</div>
<!--End of Projects Tab-->

<!--end of the tabs-->

