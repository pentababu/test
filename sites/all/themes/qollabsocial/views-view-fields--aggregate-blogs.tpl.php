<style>
h2{
color:#777;
font-size:14px;
padding:10px 0px 0px 10px;
}
.views-exposed-form .views-exposed-widget .form-submit {
margin-top: 0px;
}
#video-container {
height: 188px;
width: 220px;
border: 1px solid #CCC;
background-image: -moz-linear-gradient(center top, white90%, #F0F0F0100%);
background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0.90, white), color-stop(1.0, #F0F0F0));
}
#meta{
font-weight:normal;
width: 150px;
text-align: right;
font-size: 10px;
color: #6F6F75;
}
#video-thumb{
height: 118px;
width: 153px;
_zoom: 1;
padding: 5px;
background: white;
overflow: hidden;
border: 1px solid lightGrey;
border-radius: 3px;
-moz-border-radius: 3px;
-webkit-border-radius: 3px;
_line-height: 0;
margin:9px 0px 0px 27px;
}
#video-thumb img{
-webkit-box-shadow: 0 0px 3px black;
-moz-box-shadow: 0 0px 3px black;
}
#video-title{
font-weight: bold;
font-size: 11px;
width: 122px;
text-align: center;
margin-top:5px;
margin-left:18px;
display:inline-block;
}
#video-title a{
color: #58585D;
text-decoration:none;
}
#created-user{
height: 25px;
width: 25px;
border: none;
margin-left:10px;
display:inline-block;
opacity:0.8;
}
#username{
display:none;
}
#username a{
color: #58585D;
font-size: 10px;
margin-left:7px;
}
</style>
<div id="video-container"><div id="video-thumb"><?php print $fields['field_images_fid']->content;?></div><div id="created-user"><?php print $fields['picture']->content;?></div><div id="video-title"><?php print $fields['title']->content;?></div><div id="username"><?php print $fields['name']->content;?></div></div>
