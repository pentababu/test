<style>
.picture {
height:30px;
width:30px;
display:inline-block;
}
#user-card{
display:inline-block;
float:right;
background:#f9f9f9;
padding:10px;
font-size:11px;
}
#user-card a{
color:#417bb6;
vertical-align:top;
}
.terms {
display:inline-block;
margin-bottom:5px;
}
.terms ul li{
background:#f9f9f9;
padding:5px;
-moz-border-radius:3px;
border-radius:3px;
}
.terms ul li a{
color:#417bb6;
font-size:11px;
font-style:normal;
}
#user-info{
display:inline-block;
}
#question-links a{
color:#417bb6;
font-size:11px;
padding:2px;
margin-left:10px;
}
#question-links a:hover{
background:whitesmoke;
text-decoration:none;
}
#question-links{
vertical-align:top;
display:inline-block;
padding-top:0px;
margin-left:15px;
}
.links{
display:block;
margin-top:20px;
margin-bottom:20px;
}
#question-meta{
display:block;
font-size:11px;
color:#999;
margin-top:20px;
margin-bottom:20px;

}
#question-meta img{
vertical-align:middle;
}
#question-meta a{
color:#999;
font-size:11px;
}
.box {
margin-top:20px;
}
.box h2{
font-size:14px;
}
.clearfix {
display:block;
}
.form-item label{
display:none;
}
html.js .resizable-textarea textarea {
    display: block;
    height: 100px;
    margin-bottom: 0;
    width: 100%;
    font-size:120%;
    border:1px solid #999999;
}
</style>
  <div class="inner">
    <?php if ($page == 0): ?>
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>
    <!--<?php if ($submitted): ?>
    <div class="meta">
      <span class="submitted"><?php print $submitted ?></span>
    </div>
    <?php endif; ?>-->

    <?php if ($node_top && !$teaser): ?>
    <div id="node-top" class="node-top row nested">
      <div id="node-top-inner" class="node-top-inner inner">
        <?php print $node_top; ?>
      </div><!-- /node-top-inner -->
    </div><!-- /node-top -->
    <?php endif; ?>

    <div class="content clearfix">
    <div class="actions">
    </div>
	  <?php print $node->content["body"]["#value"]; ?>
    </div>
    <?php if ($terms): ?>
    <div class="terms">
      <?php print $terms; ?>
    </div>
    <?php endif;?>
<div id="user-card">
<?php print $picture;?>
<div id="user-info"><?php print theme('username', $node) . ' <br /> ' . 'asked on ' . format_date($node->created, 'custom', "F jS, Y") ;?></div></div>



<div id="question-meta"><span style="padding-left:5px;"><img src="/sites/all/themes/qollabsocial/images/following.png"/><span style="padding-left:5px;"><?php print $node->links['statistics_counter']['title'];?></span></span><span style="padding-left:35px;"><img src="/sites/all/themes/qollabsocial/images/answer.png"/><a href='<?php echo "/".$node->path."#node-form";?>'><?php print $node->links['comment_add']['title'];?></a></span>	
<!-- start of edit, delete links-->
<div id="question-links">
 <?php if (user_access('edit own questions') || ($user->uid == $node->uid)): ?>
      <a href="/node/<?php print $node->nid; ?>/edit" title="Edit">edit</a>
   <?php endif; ?>
 <?php if (user_access('delete own questions') || ($user->uid == $node->uid)): ?>
      <a href="/node/<?php print $node->nid; ?>/delete" title="Delete">delete</a>
   <?php endif; ?>
</div>
<!--edit,delete links-->
</div>
<div style="border-bottom:1px solid #ccc;margin-bottom:20px;"></div>
    <?php
        if($node->comment_count == 1){
            $subtitle = " Answer";
        } else {
            $subtitle = " Answers";
        }
    
    ?>
    <div class="s3 margin_top_1em">
        <h2><?php echo $node->comment_count . $subtitle ; ?></h2>
    </div>
  </div><!-- /inner -->

  <?php if ($node_bottom && !$teaser): ?>
  <div id="node-bottom" class="node-bottom row nested">
    <div id="node-bottom-inner" class="node-bottom-inner inner">
      <?php print $node_bottom; ?>
    </div><!-- /node-bottom-inner -->
  </div><!-- /node-bottom -->
  <?php endif; ?>
</div><!-- /node-<?php print $node->nid; ?> -->
