<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <style>
<!--.form-radios { display:inline-block;}.form-item label {
display: inline-block;
font-weight: normal;
vertical-align: top;
}-->
  #content-tabs ul.primary li,
#content-tabs ul.secondary li {
  border-style: none;
  display: inline-block;
  float: left; /* LTR */
  list-style: none;
  margin: 0 0px;
  padding: 0 0px;
  width:258px;
  white-space:nowrap;
  height:23px;

}
#content-tabs ul.primary li a:link,
#content-tabs ul.primary li a:visited,
#content-tabs ul.secondary li a:link,
#content-tabs ul.secondary li a:visited {
  background-color: transparent;
  border: none;
  color: #fff;
  float: left; /* LTR */
  font-weight: bold;
  margin: 0;
  margin-left:80px;
  padding: 3px 0 6px 0;
  text-decoration: none;
  white-space: nowrap;
}
#content-tabs ul.primary li.first a:link, #content-tabs ul.primary li.first a:visited {
  margin-left:94px;
}
#content-tabs ul.primary li.active a:link, #content-tabs ul.primary li.active a:visited{
    background: none repeat scroll 0 0 #577F04;
    margin-left:64px;
}
#content-tabs ul.primary li a.active:link, #content-tabs ul.primary li a.active:visited {
    color: #FFFFFF;
    padding-bottom: 2px;
/*    padding-left:30px;*/
}
#content-inner-inner {
    border-left: none;
}
  .tabContainer{margin:0;width:100%;display:inline-block;/*-moz-box-shadow: 0px 0px 8px #c4c4c4;-webkit-box-shadow: 0px 0px 8px #c4c4c4;box-shadow: 0px 0px 8px #c4c4c4;*/
}
  .tabContainer .digiTabs{list-style:none;display:block;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{text-align:center;width:86px;float:left;padding:5px!important;cursor:pointer;border-bottom:none;font-family:Lucida Grande, "Verdana";font-size:12px;font-weight:bold;color:#393939;-moz-box-shadow: 0px 0px 3px #c4c4c4;-webkit-box-shadow: 0px 0px 3px #c4c4c4;box-shadow: 0px 0px 3px #c4c4c4;margin-right:0px;}
  .tabContainer .digiTabs .selected{background-color:#fff;color:#393939;-moz-box-shadow:0px -2px 3px #C4C4C4;-webkit-box-shadow:0px -2px 3px #C4C4C4;box-shadow:0px -2px 3px #C4C4C4;}
  #tabContent{padding:25px 10px 10px 20px;background-color:#fff;float:left;margin-bottom:10px;/*border:1px solid #e1e1e1;*/width:93%;}
  .tabContainer .digiTabs li#tab8 { width:92px;}
	.cb-enable, .cb-disable, .cb-enable span, .cb-disable span { display: block; float: left;font-weight:bold;font-size:11px; }
	.cb-enable span, .cb-disable span { line-height: 20px; display: block;  font-weight: bold; }
	.cb-enable span { padding: 0 10px;background:#fafafa;color:#999;-webkit-border-radius:5px 0px 0px 5px;border:2px solid #ccc;border-right:1px solid #ccc; }
	.cb-disable span { padding: 0 10px; background:#fafafa;color:#999;-webkit-border-radius:0px 5px 5px 0px;border:2px solid #ccc;border-left:1px solid #ccc;}
	.cb-disable.selected {  }
	.cb-disable.selected span {  color: #ea48ab;background:#edf3f5;-webkit-border-radius:0px 5px 5px 0px;border: 2px solid #ccc;border-left:1px solid #ccc; }
	.cb-enable.selected {  }
	.cb-enable.selected span {  color: #ea48ab; background:#edf3f5;-webkit-border-radius:5px 0px 0px 5px;border: 2px solid #ccc;border-right:1px solid #ccc;}
	.switch label { cursor: pointer; }
form input.form-submit, form input.form-submit:hover, form input.form-submit.hover, form input.form-submit:focus {border: none;background: #78AE09;font-size: 14px;color: white;text-transform: capitalize;font-weight: bolder;border:2px solid #eee;}
form input.form-submit {margin: 2px;padding: 4px 7px;}
  </style>
  <?php print $scripts; ?>
<script type="text/javascript">
$(document).ready( function(){ 
	$(".cb-enable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', true);
	});
	$(".cb-disable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.checkbox',parent).attr('checked', false);
	});
});
</script>

</head>

<body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>">
<?php if(!user_is_logged_in()) { drupal_goto('user/login');}?>
  <div id="page" class="page">
    <div id="page-inner" class="page-inner">
      <div id="skip">
        <a href="#main-content-area"><?php print t('Skip to Main Content Area'); ?></a>
      </div>

      <!-- header-top row: width = grid_width -->
      <?php print theme('grid_row', $header_top, 'header-top', 'full-width', $grid_width); ?>

      <!-- header-group row: width = grid_width -->
	  <div class="top-strip"><div class="top-strip-wrapper"><div id="login-block"><?php
global $user;

if ( $user->uid ) {?>
  <a href="/dashboard">Dashboard</a>  |  <a href="/logout">Logout</a>
<?php }
else {?>
  <a href="/user/login">Login</a>  |   <a href="/user/register">Register</a>
<?php }
?></div></div></div>
  <div id="top-sub-strip"></div>
      <div id="header-group-wrapper" class="header-group-wrapper full-width">
        <div id="header-group" class="header-group row <?php print $grid_width; ?>">
          <div id="header-group-inner" class="header-group-inner inner clearfix">
            <?php print theme('grid_block', theme('links', $secondary_links), 'secondary-menu'); ?>
            <?php print theme('grid_block', $search_box, 'search-box'); ?>
  <div id="global-logo"><img src="/sites/all/themes/qollabsocial/images/front-logo.png" /></div>
            <?php if ($logo || $site_name || $site_slogan): ?>
            <div id="header-site-info" class="header-site-info block">
              <div id="header-site-info-inner" class="header-site-info-inner inner">
                <?php if ($logo): ?>
                <div id="logo">
                  <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
                </div>
                <?php endif; ?>
                <?php if ($site_name || $site_slogan): ?>
                <div id="site-name-wrapper" class="clearfix">
                  <?php if ($site_name): ?>
                  <span id="site-name"><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></span>
                  <?php endif; ?>
                  <?php if ($site_slogan): ?>
                  <span id="slogan"><?php print $site_slogan; ?></span>
                  <?php endif; ?>
                </div><!-- /site-name-wrapper -->
                <?php endif; ?>
              </div><!-- /header-site-info-inner -->
            </div><!-- /header-site-info -->
            <?php endif; ?>

            <?php print $header; ?>

          </div><!-- /header-group-inner -->
        </div><!-- /header-group -->
      </div><!-- /header-group-wrapper -->
<!--  <div id="top-sub-strip"></div>-->
      <!-- preface-top row: width = grid_width -->

<!--Preface top primary links-->
<div id="preface-top-wrapper" class="preface-top-wrapper full-width">
<div id="preface-top" class="preface-top row grid16-16">
            <?php print theme('grid_block', $primary_links_tree, 'primary-menu'); ?>
</div><!-- /preface-top -->
</div>
<!--End of preface top primary links-->
      <!-- main row: width = grid_width -->
      <div id="main-wrapper" class="main-wrapper full-width">
        <div id="main" class="main row <?php print $grid_width; ?>">
          <div id="main-inner" class="main-inner inner clearfix">
            <?php print theme('grid_row', $sidebar_first, 'sidebar-first', 'nested', $sidebar_first_width); ?>

            <!-- main group: width = grid_width - sidebar_first_width -->
            <div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
              <div id="main-group-inner" class="main-group-inner inner">
                <?php print theme('grid_row', $preface_bottom, 'preface-bottom', 'nested'); ?>

                <div id="main-content" class="main-content row nested">
                  <div id="main-content-inner" class="main-content-inner inner">
                    <!-- content group: width = grid_width - (sidebar_first_width + sidebar_last_width) -->
                    <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
                      <div id="content-group-inner" class="content-group-inner inner">
                        

                        <?php if ($content_top || $help || $messages): ?>
                        <div id="content-top" class="content-top row nested">
                          <div id="content-top-inner" class="content-top-inner inner">
                            <?php print theme('grid_block', $help, 'content-help'); ?>
                            <?php print theme('grid_block', $messages, 'content-messages'); ?>
                            <?php print $content_top; ?>
                          </div><!-- /content-top-inner -->
                        </div><!-- /content-top -->
                        <?php endif; ?>
			<?php if ($content_right):?>
                        <div id="content-region" class="content-region row nested">
                          <div id="content-region-inner" class="content-region-inner inner">
                            <a name="main-content-area" id="main-content-area"></a>
<?php if($node->type=='individual' || $node->type=='question'){ }else { print theme('grid_block', $tabs, 'content-tabs'); }?>
                            <div id="content-inner" class="content-inner block">
                              <div id="content-inner-inner" class="content-inner-inner inner">
                                <?php if ($title): ?>
                                <h1 class="title"><?php print $title; ?></h1>
                                <?php endif; ?>
                                <?php if ($content): ?>
                                <div id="content-content" class="content-content">
                                  <?php print $content; ?>
                                  <?php print $feed_icons; ?>
                                </div><!-- /content-content -->
                                <?php endif; ?>
                              </div><!-- /content-inner-inner -->
                            </div><!-- /content-inner -->
                          </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->
			<div id="content-right"><?php print $content_right;?></div>
                        <?php print theme('grid_row', $content_bottom, 'content-bottom', 'nested'); ?>
                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->

		<?php else: ?>
                        <div id="content-region-full" class="content-region row nested">
                          <div id="content-region-inner" class="content-region-inner inner">
                            <a name="main-content-area" id="main-content-area"></a>
			  <div class="tabContainer" ><ul class="digiTabs" id="sidebarTabs"><li  id="tab1"><a href="/manage/profile">Profile</a></li><li id="tab2"><a href="/manage/blogs">Blogs</a></li><li id="tab3"><a href="/manage/videos">Videos</a></li><li id="tab4"><a href="/manage/albums">Albums</a></li><li id="tab5" class="selected"><a href="/manage/events">Events</a></li><li id="tab6"><a href="/manage/projects">Projects</a></li><li id="tab7"><a href="/manage/groups">Groups</a></li><li id="tab8"><a href="/manage/connections">Connections</a></li></ul></div>
<?php if($node->type=='individual' || $node->type=='question'){ }else { print theme('grid_block', $tabs, 'content-tabs'); }?>
			   <div id="content-tabs" class="content-tabs block">
				<div id="content-tabs-inner" class="content-tabs-inner inner clearfix">
				 <ul class="tabs primary">
				   <li class="first"><a href="/manage/events" class="active">Organize</a></li>
				    <li class="active"><a href="/manage/events/settings">Edit Event Settings</a></li>
				    <li ><a href="/manage/events/create">Register Your Event</a></li>
				 </ul>
				</div><!-- /content-tabs-inner -->
				</div><!-- /content-tabs -->

                            <div id="content-inner" class="content-inner block">
                              <div id="content-inner-inner" class="content-inner-inner inner">
                                <?php if ($title): ?>
                                <h1 class="title"><?php print $title; ?></h1>
                                <?php endif; ?>
                                <?php if ($content): ?>
                                <div id="content-content" class="content-content">
<span style="font-size:14px;font-weight:bold;color:#ea48ab;margin-top:10px;margin-left:20px;display:block;">Event Settings</span>
<!--Table for edit settings Form -->
<form action="/manage/events/settings"  accept-charset="UTF-8" method="post" id="manageevents-data-entry-form">
<table style="width:45%;margin-left:0px;margin-top:25px;"><tbody style="border:none;"><tr style="border:none;"><td style="font-size:12px;font-weight:bold;float:right;margin-right:50px;height:30px;padding-top:15px;display:block;">Enable Comments:</td><td><span class="field switch"><label for="radio1" class="cb-enable selected"><span>Yes</span></label><label for="radio2" class="cb-disable"><span>No</span></label></span></td></tr><tr style="border:none;"><td style="font-size:12px;font-weight:bold;float:right;margin-right:50px;height:30px;padding-top:15px;display:block;">Enable Voting:</td><td><span class="field switch"><label for="radio1" class="cb-enable selected"><span>Yes</span></label><label for="radio2" class="cb-disable"><span>No</span></label></span></td></tr><tr style="border:none;margin-top:10px;"><td style="font-size:12px;font-weight:bold;vertical-align:middle;float:right;margin-right:50px;height:30px;padding-top:15px;display:block;">Link to Activity Feed:</td><td><span class="field switch"><label for="radio1" class="cb-enable selected"><span>Yes</span></label><label for="radio2" class="cb-disable"><span>No</span></label></span></td></tr><tr style="border:none;"><td style="font-size:12px;font-weight:bold;float:right;margin-right:50px;height:30px;padding-top:15px;display:block;">Enable RSS Feeds:</td><td><span class="field switch"><label for="radio1" class="cb-enable selected"><span>Yes</span></label><label for="radio2" class="cb-disable"><span>No</span></label></span></td></tr></tbody></table>
<span style="display:block; width:500px;margin-top:50px; float:right;"><input type="submit" name="op" id="edit-submit" value="Save Settings"  class="form-submit" /></span>
</form>
<!-- End of the Table -->
<!--                                  <?php print $content; ?>-->
                                  <?php print $feed_icons; ?>
                                </div><!-- /content-content -->
                                <?php endif; ?>
                              </div><!-- /content-inner-inner -->
                            </div><!-- /content-inner -->
                          </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->
                        <?php print theme('grid_row', $content_bottom, 'content-bottom', 'nested'); ?>
                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->

<?php endif;?>

                    <?php print theme('grid_row', $sidebar_last, 'sidebar-last', 'nested', $sidebar_last_width); ?>
                  </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                <?php print theme('grid_row', $postscript_top, 'postscript-top', 'nested'); ?>
              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->
          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div><!-- /main-wrapper -->

      <!-- postscript-bottom row: width = grid_width -->
      <?php print theme('grid_row', $postscript_bottom, 'postscript-bottom', 'full-width', $grid_width); ?>

      <!-- footer row: width = grid_width -->
      <?php print theme('grid_row', $footer, 'footer', 'full-width', $grid_width); ?>

      <!-- footer-message row: width = grid_width -->
      <div id="footer-message-wrapper" class="footer-message-wrapper full-width">
	<div id="bottom-sub-strip"></div>
        <div id="footer-message" class="footer-message row <?php print $grid_width; ?>">
          <div id="footer-message-inner" class="footer-message-inner inner clearfix">
            <?php print theme('grid_block', $footer_message, 'footer-message-text'); ?>
          </div><!-- /footer-message-inner -->
        </div><!-- /footer-message -->
      </div><!-- /footer-message-wrapper -->

    </div><!-- /page-inner -->
  </div><!-- /page -->
  <?php print $closure; ?>
</body>
</html>
