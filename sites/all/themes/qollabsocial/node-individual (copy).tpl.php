<style>
#profile-left {
display:inline-block;
width:15%;
vertical-align:top;
font-weight:bold;
font-size:12px;
text-transform:capitalize;
}
h1{display:none;}
.picture img{border:5px solid #eef8fe;}
</style>
<div id="profile-left"><?php $my_node_type = 'individual';
if ($node->type == $my_node_type && arg(0) == 'node' && !empty($node->picture)) {
  $picture_url = base_path() . $node->picture;
  $picture_alt = 'User\'s picture';
  print '<span class="picture">'. "\n" .'<img src="'. $picture_url .'" alt="'. $picture_alt .'" title="'. $picture_alt .'" />'. "\n" .'</span>';
}?><?php print $node->field_first_name[0]['view'] ?>&nbsp;<?php print $node->field_last_name[0]['view'] ?></div>
<!-- Tabs for the profile of an individual-->
<!--javascript-->
<script type="text/javascript">
  function tabs(x)
  {
    var lis=document.getElementById("sidebarTabs").childNodes; //gets all the LI from the UL
 
    for(i=0;i<lis.length;i++)
    {
      lis[i].className=""; //removes the classname from all the LI
    }
    x.className="selected"; //the clicked tab gets the classname selected
    var res=document.getElementById("tabContent");  //the resource for the main tabContent
    var tab=x.id;
    switch(tab) //this switch case replaces the tabContent
    {
      case "tab1":
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
      case "tab2":
        res.innerHTML=document.getElementById("tab2Content").innerHTML;
        break;
      case "tab3":
        res.innerHTML=document.getElementById("tab3Content").innerHTML;
        break;
      default:
        res.innerHTML=document.getElementById("tab1Content").innerHTML;
        break;
 
    }
  }
 
</script>
<!--end of javascript-->
<!--style for the profile pages-->
<style>
  .tabContainer{margin:10px 0;width:70%;display:inline-block;-moz-box-shadow: 0px 0px 8px #c4c4c4;-webkit-box-shadow: 0px 0px 8px #c4c4c4;box-shadow: 0px 0px 8px #c4c4c4;
}
  .tabContainer .digiTabs{list-style:none;display:block;margin:0;padding:0px;position:relative;top:1px;}
  .tabContainer .digiTabs li{text-align:center;width:99px;float:left;padding:5px!important;cursor:pointer;border-bottom:none;font-family:Lucida Grande, "Verdana";font-size:14px;font-weight:bold;color:#393939;-moz-box-shadow: 0px 0px 3px #c4c4c4;-webkit-box-shadow: 0px 0px 3px #c4c4c4;box-shadow: 0px 0px 3px #c4c4c4;margin-right:0px;}
  .tabContainer .digiTabs .selected{background-color:#fff;color:#393939;}
  #tabContent{padding:10px;background-color:#fff;float:left;margin-bottom:10px;/*border:1px solid #e1e1e1;*/width:93%;}
  .profile-bio-content { background:#f8f9f9;margin-top:5px;margin-bottom:5px;padding:5px;}
  #profile-fields-label{color:#ef53b3;font-weight:bold;font-size:12px;display:inline-block;width:100px;vertical-align:top;}
  .profile-field-item{display:inline-block;font-weight:normal;width:177px;color:#333333;}
  .profile-fields {display:inline-block;width:auto;float:left;}
  .profile-fields-right {display:inline-block;width:auto; float:right;}
</style>
<!--end of styles-->
<div class="tabContainer" >
  <ul class="digiTabs" id="sidebarTabs">
    <li  id="tab1" class="selected"  onclick="tabs(this);">General</li>
    <li id="tab2" onclick="tabs(this);">Professional</li>
    <li id="tab3"  onclick="tabs(this);">Social</li>
    <li id="tab4"  onclick="tabs(this);">Multimedia</li>
    <li id="tab5"  onclick="tabs(this);">Events</li>
    <li id="tab5"  onclick="tabs(this);">Projects</li>
  </ul>
  <div id="tabContent">

<div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">About Me</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12px;">Basic Information</div>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">First Name:</div>
      <div class="profile-field-item"><?php print $node->field_first_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Date of Birth:</div>
      <div class="profile-field-item"><?php print $node->field_dob[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Last Name:</div>
      <div class="profile-field-item"><?php print $node->field_last_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Languages:</div>
      <div class="profile-field-item"><?php print $node->field_languages[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sex:</div>
      <div class="profile-field-item"><?php print $node->field_gender[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Martial Status:</div>
      <div class="profile-field-item"><?php print $node->field_martial_status[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Here for:</div>
      <div class="profile-field-item"><?php print $node->field_here_for[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Contact No:</div>
      <div class="profile-field-item"><?php print $node->field_contact_number_p[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div style="border-bottom:1px solid #ccc;width:600px;"></div>
<div style="color:#ef53b3;font-size:12px;padding-top:5px;">Personal</div>
<table style="width:47%;display:inline-block;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Interests:</div>
      <div class="profile-field-item"><?php print $node->field_interests[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Industry:</div>
      <div class="profile-field-item"><?php print $node->field_industry[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Activities:</div>
      <div class="profile-field-item"><?php print $node->field_activities[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Political View:</div>
      <div class="profile-field-item"><?php print $node->field_political_view[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">5 things I can live without:</div>
      <div class="profile-field-item"><?php print $node->field_five_things[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sports:</div>
      <div class="profile-field-item"><?php print $node->field_sports[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Music:</div>
      <div class="profile-field-item"><?php print $node->field_music[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;">
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_indi_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
</div>
</div>
</div>
 
<div id="tab1Content" style="display:none;">
<div class="profile-bio">
  <h3 class="field-label" style="color:#ef53b3;font-size:12px;">About Me:</h3>
  <div class="profile-bio-content">
      <div class="field-item"><?php print $node->field_bio[0]['view'] ?></div>
  </div>
</div>
<div style="color:#ef53b3;font-size:12px;">Basic Information:</div>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">First Name:</div>
      <div class="profile-field-item"><?php print $node->field_first_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Date of Birth:</div>
      <div class="profile-field-item"><?php print $node->field_dob[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Last Name:</div>
      <div class="profile-field-item"><?php print $node->field_last_name[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Languages:</div>
      <div class="profile-field-item"><?php print $node->field_languages[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sex:</div>
      <div class="profile-field-item"><?php print $node->field_gender[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Martial Status:</div>
      <div class="profile-field-item"><?php print $node->field_martial_status[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Here for:</div>
      <div class="profile-field-item"><?php print $node->field_here_for[0]['view'] ?></div>
</div>
</td>
<td>
<div class="profile-fields-right">
  <div id="profile-fields-label">Contact No:</div>
      <div class="profile-field-item"><?php print $node->field_contact_number_p[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div style="border-bottom:1px solid #ccc;width:600px;"></div>
<div style="color:#ef53b3;font-size:12px;padding-top:5px;">Personal</div>
<table style="width:47%;display:inline-block;"><tbody style="border:none;"><tr><td>
<div class="profile-fields" >
  <div id="profile-fields-label">Interests:</div>
      <div class="profile-field-item"><?php print $node->field_interests[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Industry:</div>
      <div class="profile-field-item"><?php print $node->field_industry[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Activities:</div>
      <div class="profile-field-item"><?php print $node->field_activities[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Political View:</div>
      <div class="profile-field-item"><?php print $node->field_political_view[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">5 things I can live without:</div>
      <div class="profile-field-item"><?php print $node->field_five_things[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Sports:</div>
      <div class="profile-field-item"><?php print $node->field_sports[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Music:</div>
      <div class="profile-field-item"><?php print $node->field_music[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
<div id="quick-contact" style="float:right;display:inline-block;background:#f8f9f9;padding:5px;vertical-align:top;">
<table><tbody style="border:none;"><tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Email Id:</div>
      <div class="profile-field-item"><?php print $node->field_indi_email_id[0]['view'] ?></div>
</div>
</td></tr>
<tr><td>
<div class="profile-fields">
  <div id="profile-fields-label">Website:</div>
      <div class="profile-field-item"><?php print $node->field_website[0]['view'] ?></div>
</div>
</td></tr></tbody></table>
</div>
</div></div>
<div id="tab2Content" style="display:none;"><div class="field field-type-text field-field-headline">
  <h3 class="field-label">Headline</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_headline[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-job">
  <h3 class="field-label">Job Title</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_job[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-job-description">
  <h3 class="field-label">Job Description</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_job_description[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-occupation">
  <h3 class="field-label">Occupation</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_occupation[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-company">
  <h3 class="field-label">Company</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_company[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-link field-field-company-link">
  <h3 class="field-label">Company Webpage</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_company_link[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-education">
  <h3 class="field-label">Education</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_education[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-high-school">
  <h3 class="field-label">High School</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_high_school[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-date field-field-school-year">
  <h3 class="field-label">Year</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_school_year[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-college">
  <h3 class="field-label">College/University</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_college[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-number-integer field-field-year">
  <h3 class="field-label">year</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_year[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-qualification">
  <h3 class="field-label">Major / Discipline</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_qualification[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-degree">
  <h3 class="field-label">Degree</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_degree[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-past-positions">
  <h3 class="field-label">Past Positions</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_past_positions[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-exp-goals">
  <h3 class="field-label">Professional Experience & Goals</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_exp_goals[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-honor-awards">
  <h3 class="field-label">Honors & Awards</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_honor_awards[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-specialties">
  <h3 class="field-label">Specialties</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_specialties[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-career-skills">
  <h3 class="field-label">Career Skills</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_career_skills[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-career-interests">
  <h3 class="field-label">Career Interests</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_career_interests[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-work-email">
  <h3 class="field-label">Work Email Id</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_work_email[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-number-integer field-field-work-contact-number">
  <h3 class="field-label">Work Place Contact number</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_work_contact_number[0]['view'] ?></div>
  </div>
</div>
</div>
<div id="tab3Content" style="display:none;"><div class="field field-type-text field-field-s-prob">
  <h3 class="field-label">Social problems you would like solved</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_s_prob[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-s-prob-role">
  <h3 class="field-label">Where do you think you will fit in?</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_s_prob_role[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-s-work-exp">
  <h3 class="field-label">Previous/Present social work experience</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_s_work_exp[0]['view'] ?></div>
  </div>
</div>

<div class="field field-type-text field-field-work-you-like">
  <h3 class="field-label">Work you like</h3>
  <div class="field-items">
      <div class="field-item"><?php print $node->field_work_you_like[0]['view'] ?></div>
  </div>
</div></div>

<!--end of the tabs-->

