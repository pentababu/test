<style>

#view-id-user_aggregate_albums-page_1 .picture{
position:absolute;
top:235px;
height:30px;
width:30px;
border:1px solid #787880;
}
#view-id-user_aggregate_albums-page_1 h2.title{
position:absolute;
top:195px;
font-weight:normal;
font-size:16px;
width:221px;
text-align:center;
}
#view-id-user_aggregate_albums-page_1 a{
color:#58585d;
}
#view-id-user_aggregate_albums-page_1 .terms{
position:absolute;
top:175px;
font-size:12px;
width:221px;
text-align:center;
text-transform:capitalize;
}
#view-id-user_aggregate_albums-page_1 .terms a{
color:#7e9500;
}
#view-id-user_aggregate_albums-page_1 .meta{
position: absolute;
top: 235px;
margin-left: 70px;
width: 150px;
text-align: right;
font-size: 10px;
color: #6F6F75;
}
#view-id-user_aggregate_albums-page_1 .comment_add{
display:none;
}
#view-id-user_aggregate_albums-page_1 .flag-share{
display:none;
}
#view-id-user_aggregate_albums-page_1 .comment_comments{
display:none;
}
#view-id-user_aggregate_albums-page_1 .views-field-field-picture-fid{
background: url("/sites/all/themes/qollabsocial/images/album-bg.png") no-repeat;
width:182px;
height:148px;
margin:18px 0px 0px 22px;
}
#view-id-user_aggregate_albums-page_1 .views-field-field-picture-fid img{
padding:9px 0px 0px 7px;
}
#view-id-user_aggregate_albums-page_1 .views-row{
height:216px;
width:221px;
border:1px solid #ccc;
 background-image: -moz-linear-gradient(center top, rgb(255,255,255)90%, rgb(240,240,240)100%);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0.90, rgb(255,255,255)), color-stop(1.0, rgb(240,240,240)));
}
#view-id-user_aggregate_albums-page_1 .view-footer{
margin-top:50px;
}
#view-id-user_aggregate_albums-page_1 .row-1{
height:300px;
}
#view-id-user_aggregate_albums-page_1 .row-2{
height:300px;
}
</style>

<?php
// $Id: views-view-grid.tpl.php,v 1.3.4.1 2010/03/12 01:05:46 merlinofchaos Exp $
/**
 * @file views-view-grid.tpl.php
 * Default simple view template to display a rows in a grid.
 *
 * - $rows contains a nested array of rows. Each row contains an array of
 *   columns.
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<table class="views-view-grid">
  <tbody>
    <?php foreach ($rows as $row_number => $columns): ?>
      <?php
        $row_class = 'row-' . ($row_number + 1);
        if ($row_number == 0) {
          $row_class .= ' row-first';
        }
        if (count($rows) == ($row_number + 1)) {
          $row_class .= ' row-last';
        }
      ?>
      <tr class="<?php print $row_class; ?>">
        <?php foreach ($columns as $column_number => $item): ?>
          <td class="<?php print 'col-'. ($column_number + 1); ?>">
            <?php print $item; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
