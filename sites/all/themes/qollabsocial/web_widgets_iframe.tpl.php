<?php
// $Id: web_widgets_iframe.tpl.php,v 1.1 2010/04/22 06:02:39 voidfiles Exp $
/**
 * @file
 * Template for the code what to embed in external sites
 */
?>
<script type="text/javascript">
widgetContext = <?php print $js_variables ?>;
</script>
<script id="<?php print $wid ?>" src="<?php print $script_url ?>"></script>
