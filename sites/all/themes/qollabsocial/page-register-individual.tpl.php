<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/qollabsocial/css/qollabsocial-front-style.css" /> 
  <?php print $setting_styles; ?>
  <!--[if IE 8]>
  <?php print $ie8_styles; ?>
  <![endif]-->
  <!--[if IE 7]>
  <?php print $ie7_styles; ?>
  <![endif]-->
  <!--[if lte IE 6]>
  <?php print $ie6_styles; ?>
  <![endif]-->
  <?php print $local_styles; ?>
  <?php print $scripts; ?>
<style>
.form-item label {
display: inline-block;;
font-weight: normal;
}
#edit-name, #edit-pass{
width:120px;
margin-left:5px;
}
#edit-submit{
   width:80px;
   margin-left:70px;
}
.description{
display:none;
}
h2 {
padding-top:24px;
}
</style>
</head>

<body>
  <div class="top-strip"><div class="top-strip-wrapper"><div id="login-block"><?php
global $user;

if ( $user->uid ) {?>
  <a href="/dashboard">Dashboard</a>  |  <a href="/logout">Logout</a>
<?php }
else {?>
  <a href="/user/login">Login</a>  |   <a href="/user/register">Register</a>
<?php }
?></div></div></div>
  <div id="top-sub-strip"></div>
  <div class="header-strip">
  <div class="header-wrapper">
  <div id="logo"><img src="/sites/all/themes/qollabsocial/images/front-logo.png" /></div>
  </div>
  </div>
  <div class="content">
  <div class="content-wrapper">
  <div id="signup"><h2>Login</h2>
<?php
  print '<pre>';
  print var_export($form);
  print '</pre>';
?>
<?php print $content;?>
<?php dsm($content)?>

  <div id="social-share">Share with others on<br/><br/><img src="/sites/all/themes/qollabsocial/images/facebook.jpg" /><img src="/sites/all/themes/qollabsocial/images/twitter.jpg" /><img src="/sites/all/themes/qollabsocial/images/digg.jpg" /><img src="/sites/all/themes/qollabsocial/images/tecnorati.jpg" /><img src="/sites/all/themes/qollabsocial/images/stumbleupon.jpg" /></div>
  </div>
  <div class="main-content">
  <div id="intro-text">
  <b>QollabSocial</b> is a social platform for like minded people and organizations to network and interact 
with one another for the purpose of solving problems, volunteering, finding jobs and internships, to 
work for a cause they feel deeply by collaborating with each other.
  </div>
  <div class="features">
	<table id="features-left"><tr><td><img src="/sites/all/themes/qollabsocial/images/discuss.png" /></td><td>Discuss</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/blogs.png" /></td><td>Write Blogs</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/connections.png" /></td><td>Create Connections</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/find-people.png" /></td><td>Search for Organizations</td></tr></table>
	<table id="features-right"><tr><td><img src="/sites/all/themes/qollabsocial/images/multimedia.png" /></td><td>Upload Multimedia</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/group.png" /></td><td>Participate in Groups</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/q-a.png" /></td><td>Find Answers to your questions</td></tr><tr><td><img src="/sites/all/themes/qollabsocial/images/events.png" /></td><td>Organize Events</td></tr></table>
  </div>
  <div id="watch-video-button"><div style="color:#fff;padding-top:8px;font-size:14px;font-family:"Lucida Grande";"><center><b>Watch a Short Video</b></center></div></div>
  </div>
  </div>
  </div>
  <div class="bottom-strip"></div>
  <?php print $closure; ?>
</body>
</html>
