<style>
#share {
display:inline-block;
margin-left:50px;
vertical-align:top;
}
.event-info {
margin-top:20px;
font-size:11px;
text-align:center;
}
.event-info tbody {
border:none;
}
#event-description {
margin-top:20px;
}
#where {
background:#f8f8f8;
width:160px;
height:100px;
border:1px solid #ddd;
display:inline-block;
margin-left:25px;
moz-border-radius: 2px 2px 0px 0px;
-webkit-border-radius: 2px;
border-color:#CCC #DDD #EEE;
border-width:1px;
border-style:solid;
-moz-box-shadow: #ddd 0px 1px 3px;
-webkit-box-shadow: #ddd 0px 1px 3px;
}
#when {
background:#f8f8f8;
width:200px;
height:100px;
display:inline-block;
margin-left:25px;
moz-border-radius: 2px 2px 0px 0px;
-webkit-border-radius: 2px;
border-color:#CCC #DDD #EEE;
border-width:1px;
border-style:solid;
-moz-box-shadow: #ddd 0px 1px 3px;
-webkit-box-shadow: #ddd 0px 1px 3px;
}
#how-much {
background:#f8f8f8;
width:160px;
height:100px;
display:inline-block;
margin-left:25px;
moz-border-radius: 2px 2px 0px 0px;
-webkit-border-radius: 2px;
border-color:#CCC #DDD #EEE;
border-width:1px;
border-style:solid;
vertical-align:middle;
-moz-box-shadow: #ddd 0px 1px 3px;
-webkit-box-shadow: #ddd 0px 1px 3px;
}
#OpenLayers.Control.Attribution_90{
display:none;
}
#event-map {
border:2px solid #999;
}
#edit-fbss-status .form-item input, .form-item select, .form-item textarea {
border:1px solid #ddd;
height:50px;
}
.facebook-status-faded {
padding-top:5px;
padding-left:10px;
}

.picture {
display:inline-block;
margin-right:5px;
}
#event-meta {
vertical-align:top;
}
#event-meta a{
vertical-align:top;
}
#event-image {
height:158px;
width:158px;
border:1px solid #999;
}
#event-image img{
padding:4px;
-moz-box-shadow: #999 0px 1px 3px;
-webkit-box-shadow: #999 0px 1px 3px;
}
.picture img {
padding: 3px;
background: white;
border-bottom: 1px solid #D7D7D7;
border-left: 1px solid #F2F2F2;
border-right: 1px solid #F2F2F2;
}
#stream-user {
height:50px;
width:auto;
}
.people-attending h3{
padding:0 5px 5px 5px;
border-bottom:1px solid #ddd;
text-align:center;
}
</style>
<?php if($teaser) {?>
<style>.imagecache-Small { -moz-box-shadow:1px 1px 1px #ddd;-webkit-box-shadow:1px 1px 1px #ddd;}</style>
<table style="margin-bottom:8px;"><tbody style="padding-bottom:8px;display:inline-block;border-top:none;border-bottom:2px solid #ccc;"><tr style="margin-bottom:8px;"><td><span style=""><?php print $node->field_event_image[0]['view'];?></span></td><td style="vertical-align:top;padding-left:6px;"><a href="/<?php print $node->path;?>"><?php print '<span style="color:#ea48ab;font-size:12.5px;font-weight:bold;">'.$node->title.'</span>';?></a><br/><span style="font-size:11px;"><?php print format_date($created).' by '.theme('username', $node);?></span><br/><span style="height:100px;display:inline-block;"><?php print $node->field_teaser[0]['view']?></span><br/><?php print $links;?></td></tr></tbody></table>
<?php }
else {?>
<table><tbody style="border:none;"><tr><td><div id="event-image"><?php print $node->field_event_image[0]['view'] ?></div></td>
<td style="padding-left:20px;vertical-align:top;"><div id="event-info"><div id="event-meta">Organized by: <?php print $picture;?><?php print theme('username', $node)?>   <div id="share"><?php print $node->links['flag-share']['title'];?><a href="<?php print "/node/".$node->nid."/signups/add";?>"/>Sign Up</a></div></div><br>
<div id="event-teaser"><?php print $node->field_teaser[0]['view'] ?></div></div></td></tr></tbody></table>
<div class="event-info"><table><tbody><tr><td><div id="where"><span style="font-size:13px;padding-top:10px;height:10px;display:block;"><center>Where?</center></span><span style="vertical-align:middle;height:70px;display:table-cell;width:160px;"><center><?php print $node->field_event_location[0]['view'] ?></center></span></div></td><td><div id="when"><span style="font-size:13px;padding-top:10px;height:20px;display:block;"><center>When?</center></span><span style="vertical-align:middle;height:70px;display:table-cell;width:200px;"><center><?php print $node->field_date[0]['view'] ?></center></span></div></td><td><div id="how-much"><span style="font-size:13px;padding-top:10px;height:20px;width:160px;display:block;"><center>How much?</center></span><span style="vertical-align:middle;height:70px;display:table-cell;width:160px;"><center><?php print $node->field_cost[0]['view'] ?></center></span></div></td></tr></tbody></table></div>
<table style="margin-top:10px;"><tbody style="border:none;"><tr><td style="width:70%;"><div id="event-description"><span style="font-size:13px;display:block;">Event Description:</span><?php print $node->content['body']['#value'];?></div></td>
<td><div id="event-map"><?php print $node->field_map[0]['view'];?></div></td></tr></tbody></table>
<table><tbody style="border:none;"><tr><td style="width:70%";>
<!-- Print Status Update Block Programitically -->
<?php
$block = module_invoke('facebook_status', 'block', 'view', facebook_status);
print "<h3>Discuss</h3>";
print $block['content'];
?>

<!-- Print the activity stream of the event page-->
<?php
  $display_id = 'block_1';
  $view = views_get_view('facebook_status_stream');
	if (!empty($view)) {
	  $result = $view->execute_display($display_id);
	  print $result["content"];
  }
?></td>
<td style="width:28%;vertical-align:top;padding-top:20px;">
<!-- Retrieve the user signups for the event -->
<?php
  $view_args = array($node->nid);
  $display_id = 'block_1';
  $view = views_get_view('signup_user_list');
       if (!empty($view)) {
        $result = $view->execute_display($display_id , $view_args);?>
	<span class="people-attending"><?php print "<h3>".$result["subject"]."</h3>";?></span>
<?php	print $result["content"];

  }
?>
</td></tr></tbody></table><?php }?>
