<style>
.actions{
display:inline-block;
width:9%;
}
.answer-body{
display:inline-block;
vertical-align:top;
width:90%;
}
#answer{
border-bottom:1px solid #ccc;
margin-top:10px;
}
.answer-meta{
display:inline-block;
margin-top:20px;
}
.answer-meta img{
height:25px;
width:117px;
}
#node-links a{
color:#417bb6;
font-size:11px;
padding:2px;
margin-left:10px;
}
#node-links a:hover{
background:whitesmoke;
text-decoration:none;
}
#node-links{
vertical-align:top;
display:inline-block;
padding-top:5px;
}
.inline-comment{
    border-bottom:1px solid #E5E5E5;
    background-color:whiteSmoke;
    width:96%;
    margin:15px 0px;
    padding:5px 10px 5px 10px;
    -moz-border-radius:5px;
    border-radius:5px;
    font-size:90%;
}
.inline-comment .by {
    text-align:right;
    font-size:11px;
}
.inline-comment p {
    padding:1px;
}
.inline-comment a{
color:#417bb6;
}
.inline-comments-form .form-text{
border:1px solid #999;
font-style:italic;
font-size:90%;
}
.inline-comments-form .form-submit {
border:1px solid #999;
}
.node-form .form-submit{
border:1px solid #a6c600;
background:#a6c600;
color:#fff;
font-size:12px;
}
.tips{
display:none;
}
</style>
<?php ?><div id="answer">
    <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?> clearfix margin_bottom_1em padding_bottom_halfem">
        <div class="actions">
            <?php print $vote_storylink_via; ?>
<?php print $node->content['vud_node_widget_display']['#value'];?>
        </div>
        <div class="answer-body"><?php print $node->content["body"]["#value"]; ?></div>
        <div class="context clearfix">
            <div class="tags context_item">
                <?php echo $terms; ?>
            </div>
<!--start of edit,delete-->
            <div class="answer-meta">
            <?php echo $node->links["flag-favorite"]["title"] ?>
		<div id="node-links">
            <?php if($node->links && $node->links["comment_edit"]): ?>
                <a href="/<?php echo $node->links["comment_edit"]["href"] ?>">
                    <?php echo $node->links["comment_edit"]["title"] ?>
                </a>
            <?php endif; ?>
            <?php if($node->links && $node->links["comment_delete"]): ?>
                <a href="/<?php echo $node->links["comment_delete"]["href"]; ?>?<?php echo $node->links["comment_delete"]["query"]; ?>">
                    <?php echo $node->links["comment_delete"]["title"]; ?>
                </a>
            <?php endif; ?></div>
            </div>
<!--end of edit,delete-->
                <div class="usercard clearfix">
                    <div class="left">
                        <a href='<?php echo $homepage; ?>'>
                        <?php print $picture;?>
                        </a><?php echo $homepage;?>

                    </div>
                    <div class="right">
                        <div class="name"><a href=/users/<?php echo $node->name;?>><?php echo $node->name; ?></a><div class="submitted margin_bottom_1em"><?php echo ago($node->created); ?></div></div>
                        <div>

                        </div>
                </div>
                
                <?php //echo $submitted; ?>
            </div>
        </div>
        <div class="comments">
            <div id="<?php echo $nid; ?>-comments" class="all-comments">
            <?php
                $view = views_get_view('inline_comments_for');
				if($view){
	                $view->set_arguments(array($nid));
	                $view->execute();
	                if (count($view->result)) {
	                    foreach($view->result as $comment ){
	                        $node = node_load($comment->nid);
	                        ?>
	                        <div class="inline-comment">
	                            <p><?php echo $node->body; ?></p> 
	                            <p class="by">by <a href="/user/<?php echo $node->uid ?>/<?php echo $node->name; ?>"><?php echo $node->name; ?></a> <?php echo ago($node->created); ?></p>
	                        </div>
	                        <?php
	                    }

	                }
				}

            ?>
            </div>
            <?php if($logged_in):?>
            <?php echo simple_inline_comments($nid . "-comments", $nid); ?>
            <?php endif;?>
        </div>
    </div></div>
