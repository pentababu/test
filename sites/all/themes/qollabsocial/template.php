<?php 
function qollabsocial_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' > ', $breadcrumb) .'</div>';
  }
}

//script to convert timestamp to "time ago"
function time_ago($tm,$rcs = 0) {

$cur_tm = time(); 

$dif = $cur_tm-$tm;

$pds = array('second','minute','hour','day','week','month','year','decade');

$lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);

for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

$no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
return $x;

}

function ago($timestamp){
   $difference = time() - $timestamp;
   $periods = array("second", "minute", "hour", "day", "week", "month", "years", "decade");
   $lengths = array("60","60","24","7","4.35","12","10");
   for($j = 0; $difference >= $lengths[$j]; $j++)
   $difference /= $lengths[$j];
   $difference = round($difference);
   if($difference != 1) $periods[$j].= "s";
   $text = "$difference $periods[$j] ago";
   return $text;
}

/**
 * Comment preprocessing
 */
function qollabsocial_preprocess_comment(&$vars) {
  global $user;
  static $comment_odd = TRUE;                                                                             // Comment is odd or even

  // Build array of handy comment classes
  $comment_classes = array();
  $comment_classes[] = $comment_odd ? 'odd' : 'even';
  $comment_odd = !$comment_odd;
  $comment_classes[] = ($vars['comment']->status == COMMENT_NOT_PUBLISHED) ? 'comment-unpublished' : '';  // Comment is unpublished
  $comment_classes[] = ($vars['comment']->new) ? 'comment-new' : '';                                      // Comment is new
  $comment_classes[] = ($vars['comment']->uid == 0) ? 'comment-by-anon' : '';                             // Comment is by anonymous user
  $comment_classes[] = ($user->uid && $vars['comment']->uid == $user->uid) ? 'comment-mine' : '';         // Comment is by current user
  $node = node_load($vars['comment']->nid);                                                               // Comment is by node author
  $vars['author_comment'] = ($vars['comment']->uid == $node->uid) ? TRUE : FALSE;
  $comment_classes[] = ($vars['author_comment']) ? 'comment-by-author' : '';
  $comment_classes = array_filter($comment_classes);                                                      // Remove empty elements
  $vars['comment_classes'] = implode(' ', $comment_classes);                                              // Create class list separated by spaces

  // Date & author
  $submitted_by = '<span class="comment-name">'.  theme('username', $vars['comment']) .'</span>';
  $submitted_by .= '<span class="comment-date">'.  time_ago($vars['comment']->timestamp).' '.t('ago').'</span>';     // Format date as small, medium, or large
  $vars['submitted'] = $submitted_by;
}

function qollabsocial_item_list($items = array(), $title = NULL, $type = 'ul', $attributes = NULL) {
  $output = '<div class="item-list">';
  if (isset($title)) {
    $output .= '<h3>'. $title .'</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type". drupal_attributes($attributes) .'>';
    $num_items = count($items);
    $c = $num_items;
    foreach ($items as $i => $item) {
    $c--;
      $attributes = array();
      $children = array();
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
         $data = $item;
      }
      if(!is_numeric($i)){
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      if ($c == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] .' first');
      }
      if ($c == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] .' last');
      }
      $attributes['class'] = $attributes['class'].' ' . ($c % 2 ? 'even' : 'odd');
      $output .= '<li'. drupal_attributes($attributes) .'>'. $data ."</li>\n";
      } else {
      if (count($children) > 0) {
        $data .= theme_item_list($children, NULL, $type, $attributes); // Render nested list
      }
      if ($i == 0) {
        $attributes['class'] = empty($attributes['class']) ? 'first' : ($attributes['class'] .' first');
      }
      if ($i == $num_items - 1) {
        $attributes['class'] = empty($attributes['class']) ? 'last' : ($attributes['class'] .' last');
      }
      $attributes['class'] = $attributes['class'].' ' . ($i % 2 ? 'even' : 'odd');
      $output .= '<li'. drupal_attributes($attributes) .'>'. $data ."</li>\n";
    }
    }
    $output .= "</$type>";
  }
  $output .= '</div>';
  return $output;
}

function _phptemplate_variables($hook, $vars) {
  switch ($hook) {
    case 'node':
      $vars['storylink_url'] = check_url($vars['node']->vote_storylink_url);
      if (arg(1) != 'add' && arg(2) != 'edit') {
        $style = variable_get('vote_up_down_widget_style_node', 0) == 1 ? '_alt' : '';
        $vars['vote_up_down_widget'] = theme("vote_up_down_widget$style", $vars['node']->nid, 'node');
        $vars['vote_up_down_points'] = theme("vote_up_down_points$style", $vars['node']->nid, 'node');
      }
      $vars['vote_storylink_via'] = theme('vote_storylink_via', $vars['node']->vote_storylink_url);
      if (arg(1) == 'top') {
        static $count;
        $count = is_array($count) ? $count : array();
        $count[$hook] = is_int($count[$hook]) ? $count[$hook] : 1;
        $vars['seqid'] = $count[$hook]++;
      }
      break;
  }
  return $vars;
}
/**
 * Renders comment(s) without forms.
 *
 * @param $node
 *   The node which comment(s) needs rendering.
 * @param $cid
 *   Optional, if given, only one comment is rendered.
 *
 * @see comment_render.
 */
function comment_display_comment_render_without_form($node, $cid = 0) {
  global $user;

  $output = '';

  if (user_access('access comments')) {
    // Pre-process variables.
    $nid = $node->nid;
    if (empty($nid)) {
      $nid = 0;
    }

    $mode = _comment_get_display_setting('mode', $node);
    $order = _comment_get_display_setting('sort', $node);
    $comments_per_page = _comment_get_display_setting('comments_per_page', $node);

    if ($cid && is_numeric($cid)) {
      // Single comment view.
      $query = 'SELECT c.cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp, c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature, u.picture, u.data, c.status FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE c.cid = %d';
      $query_args = array($cid);
      if (!user_access('administer comments')) {
        $query .= ' AND c.status = %d';
        $query_args[] = COMMENT_PUBLISHED;
      }

      $query = db_rewrite_sql($query, 'c', 'cid');
      $result = db_query($query, $query_args);

      if ($comment = db_fetch_object($result)) {
        $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
        $links = module_invoke_all('link', 'comment', $comment, 1);
        drupal_alter('link', $links, $node);

        $output .= theme('comment_view', $comment, $node, $links);
      }
    }
    else {
      // Multiple comment view
      $query_count = 'SELECT COUNT(*) FROM {comments} c WHERE c.nid = %d';
      $query = 'SELECT c.cid as cid, c.pid, c.nid, c.subject, c.comment, c.format, c.timestamp, c.name, c.mail, c.homepage, u.uid, u.name AS registered_name, u.signature, u.picture, u.data, c.thread, c.status FROM {comments} c INNER JOIN {users} u ON c.uid = u.uid WHERE c.nid = %d';

      $query_args = array($nid);
      if (!user_access('administer comments')) {
        $query .= ' AND c.status = %d';
        $query_count .= ' AND c.status = %d';
        $query_args[] = COMMENT_PUBLISHED;
      }

      if ($order == COMMENT_ORDER_NEWEST_FIRST) {
        if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
          $query .= ' ORDER BY c.cid DESC';
        }
        else {
          $query .= ' ORDER BY c.thread DESC';
        }
      }
      else if ($order == COMMENT_ORDER_OLDEST_FIRST) {
        if ($mode == COMMENT_MODE_FLAT_COLLAPSED || $mode == COMMENT_MODE_FLAT_EXPANDED) {
          $query .= ' ORDER BY c.cid';
        }
        else {
          // See comment above. Analysis reveals that this doesn't cost too
          // much. It scales much much better than having the whole comment
          // structure.
          $query .= ' ORDER BY SUBSTRING(c.thread, 1, (LENGTH(c.thread) - 1))';
        }
      }
      $query = db_rewrite_sql($query, 'c', 'cid');
      $query_count = db_rewrite_sql($query_count, 'c', 'cid');

      // Start a form, for use with comment control.
      $result = pager_query($query, $comments_per_page, 0, $query_count, $query_args);

      $divs = 0;
      $num_rows = FALSE;
      $comments = '';
      drupal_add_css(drupal_get_path('module', 'comment') .'/comment.css');
      while ($comment = db_fetch_object($result)) {
        $comment = drupal_unpack($comment);
        $comment->name = $comment->uid ? $comment->registered_name : $comment->name;
        $comment->depth = count(explode('.', $comment->thread)) - 1;

        if ($mode == COMMENT_MODE_THREADED_COLLAPSED || $mode == COMMENT_MODE_THREADED_EXPANDED) {
          if ($comment->depth > $divs) {
            $divs++;
            $comments .= '<div class="indented">';
          }
          else {
            while ($comment->depth < $divs) {
              $divs--;
              $comments .= '</div>';
            }
          }
        }

        if ($mode == COMMENT_MODE_FLAT_COLLAPSED) {
          $comments .= theme('comment_flat_collapsed', $comment, $node);
        }
        else if ($mode == COMMENT_MODE_FLAT_EXPANDED) {
          $comments .= theme('comment_flat_expanded', $comment, $node);
        }
        else if ($mode == COMMENT_MODE_THREADED_COLLAPSED) {
          $comments .= theme('comment_thread_collapsed', $comment, $node);
        }
        else if ($mode == COMMENT_MODE_THREADED_EXPANDED) {
          $comments .= theme('comment_thread_expanded', $comment, $node);
        }

        $num_rows = TRUE;
      }
      while ($divs-- > 0) {
        $comments .= '</div>';
      }

      $output .= $comments;
      $output .= theme('pager', NULL, $comments_per_page, 0);
    }

    $output = theme('comment_wrapper', $output, $node);
  }

  return $output;
}

function phptemplate_preprocess_node(&$vars) {
    $vars['template_files'][] = 'node-' . $vars['nid'];
    return $vars;
}

function qollabsocial_preprocess_node(&$vars) {
  $vars['comments'] = '';
  $vars['comments_form'] = '';
  $vars['comments_control'] = '';
  if (function_exists('comment_render') && !empty($vars['node']) && $vars['node']->comment) {
    $arg2 = arg(2);
    $vars['comments'] .= comment_display_comment_render_without_form($vars['node'], ($arg2 && is_numeric($arg2) ? $arg2 : NULL));
    $vars['comments_form'] .= comment_display_comment_form_render($vars['node'], 'comment_form');
    $vars['comments_controls'] .= comment_display_comment_form_render($vars['node'], 'comment_controls');

    // Reconstruct CSS and JS variables.
    $vars['css'] = drupal_add_css();
    $vars['styles'] = drupal_get_css();
    $vars['scripts'] = drupal_get_js();

  }
}
/**
 * Renders a comment form.
 *
 * @param $node
 *   The node which comment(s) needs rendering.
 * @param $form
 *   Which form should be rendered: 'comment_form' or 'comment_controls'.
 */
function comment_display_comment_form_render($node, $form = 'comment') {
  $output = '';
  $nid = $node->nid;

  $mode = _comment_get_display_setting('mode', $node);
  $order = _comment_get_display_setting('sort', $node);
  $comments_per_page = _comment_get_display_setting('comments_per_page', $node);

  if ($form == 'comment_form') {
    // If enabled, show new comment form if it's not already being displayed.
    $reply = arg(0) == 'comment' && arg(1) == 'reply';
    if (user_access('post comments') && node_comment_mode($nid) == COMMENT_NODE_READ_WRITE && (variable_get('comment_form_location_'. $node->type, COMMENT_FORM_SEPARATE_PAGE) == COMMENT_FORM_BELOW) && !$reply) {
      $output .= comment_form_box(array('nid' => $nid), t('Post new comment'));
    }
  }
  elseif ($form == 'comment_controls') {
    $output .= drupal_get_form('comment_controls', $mode, $order, $comments_per_page);
  }

  return $output;
}

function qollabsocial_preprocess_flag(&$vars) {
$temp = base_path();
  $image_file = '/sites/all/themes/qollabsocial' . '/images/flag-' . $vars['flag']->name . '-' . ($vars['action'] == 'flag' ? 'off' : 'on') . '.png';
  // Uncomment the following line when debugging.
//  drupal_set_message("Flag is looking for '$image_file''$temp'...");
//  if (file_exists($image_file)) {
    $vars['link_text'] = '<img src="' . $image_file . '" />';
  //}
}

function qollabsocial_preprocess_page(&$vars) {
  // Generate links tree & add Superfish class if dropdown enabled, else make standard primary links
  $vars['primary_links_tree'] = '';
  if ($vars['primary_links']) {
    if (theme_get_setting('primary_menu_dropdown') == 1) {
      $vars['primary_links_tree'] = menu_tree(variable_get('menu_primary_links_source', 'primary-links'));
      $vars['primary_links_tree'] = preg_replace('/<ul class="menu/i', '<ul class="menu sf-menu sf-navbar', $vars['primary_links_tree'], 1);
    }
    else {
      $vars['primary_links_tree'] = theme('links', $vars['primary_links'], array('class' => 'menu'));
    }
  }

 //allow template suggestions based on url paths.  
 $alias = drupal_get_path_alias(str_replace('/edit','',$_GET['q'])); 
 if ($alias != $_GET['q']) { 
 $suggestions = array();
 $template_filename = 'page'; 
 foreach (explode('/', $alias) as $path_part) {
  $template_filename = $template_filename . '-' . $path_part; 
  $suggestions[] = $template_filename;
  } 
 $alias_array = explode('/', $alias);
 $variables['template_files'] = $suggestions;
 }
 if (arg(0) == 'blog') {
        $vars['head_title'] = str_replace('Blogs', 'Aggregate Blogs', $vars['head_title']);
	$vars['title'] = str_replace('Blogs', 'Aggregate Blogs', $vars['title']);
 }
}

/* Declaring Node Edit/Add forms theme functions */
function qollabsocial_theme() {
  return array(
	'blog_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'question_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'event_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'video_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'album_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'picture_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'group_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'manageblog_data_entry_form' => array(
	  'arguments' => array('form' => NULL),
	),
	'manageevents_data_entry_form' => array(
	  'arguments' => array('form' => NULL),
	),
        'views_bulk_operations_form__1' => array(
	  'arguments' => array('form' => NULL),
	),
	'individual_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
        'node_gallery_gallery_node_form' => array(
	  'arguments' => array('form' => NULL),
	),
  );
}
/** Overiding the blog creation form **/

function qollabsocial_blog_node_form($form) {
//  dsm($form);
  // Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body text
  unset($form['body_field']['body']['#title']);

  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  //$published = drupal_render($form['options']['status']);
  $teaser_image = drupal_render($form['field_teaser_image']);
  $images = drupal_render($form['group_images']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);
//  $everything_else = drupal_render($form);
  return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.$teaser_image.'<div id="form-body">'.$body.'<div id="body-format">'.$body_filter.'</div></div>'.$images.'<span style="display:none;">'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/** Overiding the album creation form **/

function qollabsocial_node_gallery_gallery_node_form($form) {
//  dsm($form);
  // Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body text
  unset($form['body_field']['body']['#title']);

  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  //$published = drupal_render($form['options']['status']);
  $teaser_image = drupal_render($form['field_teaser_image']);
  $images = drupal_render($form['group_images']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);
//  $everything_else = drupal_render($form);
  return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.$teaser_image.'<div id="form-body">'.$body.'<div id="body-format">'.$body_filter.'</div></div>'.$images.'<span style="display:none;">'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}


/* Overiding the question creation form */

function qollabsocial_question_node_form($form) {
//  dsm($form);
  // Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
    // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body label text
  unset($form['body_field']['body']['#title']);
  $form['body_field']['body']['#title'] = t('Enter Your Question here');
  $form['body_field']['body']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['body_field']['body']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['body_field']['body']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);
    return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$body.'<div id="body-format">'.$body_filter.'</div></div><span style="display:none";>'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/* Overiding the video creation form */

function qollabsocial_video_node_form($form) {
  //dsm($form);
  //Modify title text
  unset($form['title']['#title']);
  //Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }

  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body label text
  unset($form['field_video_embed']['0']['embed']['#title']);
  unset($form['field_video_embed']['0']['embed']['#description']);
  $form['field_video_embed']['0']['embed']['#description'] = t('Enter third party video url here. (eg. http://www.youtube.com/video/1234)');

    //Set a default value for the title field
  $form['field_video_embed']['#title'] = NULL;
  $form['field_video_embed']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['field_video_embed']['#title']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['field_video_embed']['#title']."') {this.value = '';} this.style.color = '#333333';" );

  $title = drupal_render($form['title']);
  $video_embed = drupal_render($form['field_video_embed']);
  $field_info = drupal_render($form['group_description']);
  $tags = drupal_render($form['taxonomy']['tags']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);
   return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$video_embed.'<div id="body-format"><div id="body-video-format">'.$field_info.'</div>Terms & Conditions come here.To-Do</div></div><span style="display:none;>'.$remaining_form.'</span></td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/* Overiding the event creation form */

function qollabsocial_event_node_form($form) {
//  dsm($form);
  // Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );

  // Modify body label text
  unset($form['body_field']['body']['#title']);
  // Set a default value for the title field
  $body_title_value =   $form['body_field']['body']['#value'];
  if($body_title_value ==''){
  $form['title']['#value'] = t('Enter Event Description'); 
}
  else { }
  $form['body_field']['body']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['body_field']['body']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['body_field']['body']['#value']."') {this.value = '';} this.style.color = '#333333';" );

  // Modify teaser label text
  unset($form['field_teaser']['0']['value']['#title']);
  // Set a default value for the title field
  $teaser_title_value =   $form['field_teaser']['0']['value']['#value'];
  if($teaser_title_value ==''){
  $form['field_teaser']['0']['value']['#value'] = t('Enter Event Teaser'); 
}
  else { }
  $form['field_teaser']['0']['value']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['field_teaser']['0']['value']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['field_teaser']['0']['value']['#value']."') {this.value = '';} this.style.color = '#333333';" );

  $teaser = drupal_render($form['field_teaser']);
  unset($form['group_event_details']['field_date']['0']['#title']);
  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['signup']['#collapsed'] = FALSE;
  $form['signup']['#collapsible'] = FALSE;
  $form['signup']['signup_user_reg']['#processed'] = FALSE;
  $form['signup']['signup_enabled']['#attributes'] = array('class' => 'field switch');
  $form['signup']['signup_enabled']['1']['#attributes'] = array('class' => 'cb-enable selected');
  $form['signup']['signup_enabled']['0']['#attributes'] = array('class' => 'cb-disable');
  $form['body_field']['format']['#title'] = NULL;
  $form['group_event_details']['field_date']['0']['value']['#title'] = t('Start Date');
  $form['group_event_details']['field_date']['0']['value2']['#title'] = t('End Date');
  $event_details = drupal_render($form['group_event_details']);
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $signup = drupal_render($form['signup']);
  $form['signup']['#title'] = $form['signup']['#title'].'<a href="#" onMouseOver="toggleDiv'."('helptext',".'1)"'.' onMouseOut="toggleDiv'."('helptext',0.".')"><span style="display:inline-block;float:right;margin-right:20px;"><img src="/sites/all/themes/qollabsocial/images/help-text.png"/></span></a><div id="helptext">If enabled, you can control whether users may sign up. Other signup-related settings can be modified at the Signups: Settings tab, in your event.</div>
';
  $signup_title = $form['signup']['#title'];
  $group_images = drupal_render($form['group_images']);
  $field_country = drupal_render($form['field_country']);
  $remaining_form = drupal_render($form);
    return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$event_details.$teaser.$body.'<div id="body-format">'.$body_filter.'</div></div>'.$group_images.'<span style="display:none;">'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;"><div id="signup-events"><span style="margin-left:10px;font-size:12px;">'.$signup_title.'</span>'.$signup.'</div>'.$thematic_areas.$tags.$field_country.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/* Overiding the group creation form */

function qollabsocial_group_node_form($form) {
  dsm($form);
  // Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Title Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );




  //Modify body label text
 // unset($form['body_field']['body']['#title']);
//  $form['body_field']['body']['#value'] = t('Enter Group Description'); 
  //$form['body_field']['body']['#attributes'] = array(
    //   'onblur' => "if (this.value == '') {this.value = '".$form['body_field']['body']['#value']."';} this.style.color = '#333333';",
      //   'onfocus' => "if (this.value == '".$form['body_field']['body']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  // Modify Website text
  unset($form['group_attributes']['field_group_link']['#title']);
  unset($form['group_attributes']['field_group_link']['0']['#title']);
  unset($form['group_attributes']['field_group_link']['0']['title']['#title']);
  unset($form['group_attributes']['field_group_link']['0']['url']['#title']);
  // Set a default value for the title field
  $website_title_value = $form['group_attributes']['field_group_link']['0']['title']['#value'];
  $form['group_attributes']['field_group_link']['0']['title']['#description'] = t('Enter title of the website');
  $form['group_attributes']['field_group_link']['0']['url']['#description'] = t('Enter url of the website');
  if($website_title_value ==''){
  $form['group_attributes']['field_group_link']['0']['title']['#value'] = t('Website Title'); 
}
  else { }
  $form['group_attributes']['field_group_link']['0']['title']['#value'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['group_attributes']['field_group_link']['0']['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['group_attributes']['field_group_link']['0']['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );


  //Modify teaser label text

  unset($form['field_teaser']['0']['value']['#title']);
  $form['field_teaser']['0']['value']['#value'] = t('Enter Event Teaser.'); 
  $form['field_teaser']['0']['value']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['field_teaser']['0']['value']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['field_teaser']['0']['value']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  $form['og_directory']['#description'] = t('Disable if you want your group to be private.');
  $form['og_private']['#description'] = t('Your group will not be listed in public listings');
  unset($form['og_selective']['#description']);
  $form['og_selective']['0']['#title'] = t('Open');
  $form['og_selective']['1']['#title'] = t('Moderated');
  $form['og_selective']['2']['#title'] = t('Invite Only');
  $form['og_selective']['3']['#title'] = t('Closed');
  $teaser = drupal_render($form['field_teaser']);
  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $group_attributes = drupal_render($form['group_attributes']);
  $og_description = drupal_render($form['og_description']);
  $og_directory = drupal_render($form['og_directory']);
  $og_private = drupal_render($form['og_private']);
  $og_selective = drupal_render($form['og_selective']);
  $group_location = drupal_render($form['field_group_location']);
  $group_language = drupal_render($form['field_language']);
  $buttons = drupal_render($form['buttons']);

//  $signup_title = $form['signup']['#title'];
//  $group_images = drupal_render($form['group_images']);
  $remaining_form = drupal_render($form);
    return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$group_attributes.$og_description.$body.'<div id="body-format">'.$body_filter.'</div></div>'.$group_images.'<span style="display:none;">'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.$group_location.$group_language.$og_directory.$og_private.$og_selective.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/*Overiding the album creation form */
function qollabsocial_album_node_form($form) {
//  dsm($form);
// Modify title text
  unset($form['title']['#title']);
  // Set a default value for the title field
  $title_value =   $form['title']['#value'];
  if($title_value ==''){
  $form['title']['#value'] = t('Enter Album Name Here'); 
}
  else { }
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body label text
  unset($form['body_field']['body']['#title']);

  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);
    return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$body.'<div id="body-format">'.$body_filter.'</div></div><span style="display:none";>'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; 
}

/* Overiding the picture upload form */
function qollabsocial_picture_node_form($form) {
  dsm($form);
}
/* Overiding the blog settings form */
function qollabsocial_manageblog_data_entry_form($form) {
//  dsm($form);
  global $user;
  $uid = $user->uid;
  $query_result = db_result(db_query("SELECT uid FROM {manageblog} WHERE uid = %d", $uid));
  if ($query_result) {
    $form_title = 'Edit Blog Settings';
  }
  else {
    $form_title = 'Register Your Blog';
  }
  unset($form['title']['#title']);
  unset($form['description']['#title']);
  // Set a default value for the title field
  $form['title']['#title'] = t('Blog Name');
  $form['description']['#title'] = t('Blog Description');
  $title = drupal_render($form['title']);
  $description = drupal_render($form['description']);
  $remaining_form = drupal_render($form);
  if($query_result) {
    return '<span style="font-size:14px;font-weight:bold;color:#333;margin-bottom:10px;margin-left:20px;display:block;">'.$form_title.'</span><div id="settings-form"><div id="form-title">'.$title.'</div><div id="form-body">'.$description.'</div></div><div id="publish-post"><div id="publish-blog">'.$remaining_form.'</div></div>';
  }
  else {
  return '<span style="font-size:14px;font-weight:bold;color:#ea48ab;margin-bottom:10px;margin-left:20px;display:block;">'.$form_title.'</span>'.'<style>td {padding:7px;color:#333;line-height:175%;font-weight:bold;}</style><table style="margin:10px;padding-top:40px;height:175px;"><tbody style="border:none;"><tr><td style="margin-left:5px;width:33%;"><table><tbody style="border:none;"><tr><td style="vertical-align:top;"><img src="/sites/all/themes/qollabsocial/images/intro-write.png"/></td><td style="vertical-align:top;margin-left:3px;"><span style="color:#ea48ab;font-size:12px;font-weight:bold;">Voice out your opinion</span><br/>A blog is the best bet for a voice among the online crowd. Blogs can be used to describe events, graphic material or experiences.</td></tr></tbody></table></td><td style="margin-left:5px;width:34%;"><table><tbody style="border:none;"><tr><td style="vertical-align:top;"><img src="/sites/all/themes/qollabsocial/images/intro-vote.png"/></td><td style="vertical-align:top;margin-left:3px;"><span style="color:#ea48ab;font-size:12px;font-weight:bold;">Get Feedback from community</span><br/>Blogs let you gather opinion and feedback from the community, friends and organizations.</td></tr></tbody></table></td><td style="margin-left:5px;width:33%;"><table><tbody style="border:none;"><tr><td style="vertical-align:top;"><img src="/sites/all/themes/qollabsocial/images/intro-market.png"/></td><td style="vertical-align:top;margin-left:3px;"><span style="color:#ea48ab;font-size:12px;font-weight:bold;">Update your followers</span><br/>Blogs are a great way for an organizations or an individual to update what is happening around them descriptively.</td></tr></tbody></table></td></tr></tbody></table><hr style="margin:0 5px 0 5px;height:1px;background:#c4c4c4;border:none;"/>'.'<div id="settings-form"><div id="form-title">'.$title.'</div><div id="form-body">'.$description.'</div></div><div id="publish-post"><div id="publish-blog">'.$remaining_form.'</div></div>';
  }

}

/* Overiding the event settings form */
function qollabsocial_manageevents_data_entry_form($form) {
//  dsm($form);
  global $user;
  $uid = $user->uid;
  $query_result = db_result(db_query("SELECT uid FROM {manageevents} WHERE uid = %d", $uid));
  if ($query_result) {
    $form_title = 'Edit Event Settings';
  }
  else {
    $form_title = 'Register for Event services';
  }
  $remaining_form = drupal_render($form);
  if($query_result) {
    return '<span style="font-size:14px;font-weight:bold;color:#333;margin-bottom:10px;margin-left:20px;display:block;">'.$form_title.'</span><span style="margin-left:100px;margin-top:50px;display:block;">'.$remaining_form.'</span>';
  }
  else {
    return '<span style="font-size:14px;font-weight:bold;color:#333;margin-bottom:10px;margin-left:20px;display:block;">'.$form_title.'</span><span style="margin-left:100px;margin-top:50px;display:block;">'.$remaining_form.'</span>';  }

}


/* Overiding Bulk Operations Form (bulk operations is used in the manage section) */
 function qollabsocial_views_bulk_operations_form__1($form) {
  //dsm($form);
  $form['select']['operation']['#options']['0'] = t('Perform Action');
//  $form['select']['submit']['#title'] = t('Apply');
  $remaining_form = drupal_render($form);
  return $remaining_form;
}

/** Overiding the blog creation form **/

function qollabsocial_individual_node_form($form) {
  dsm($form);
  // Modify title text
 // unset($form['title']['#title']);
  // Set a default value for the title field
  /*$form['title']['#value'] = t('Enter Title Here'); 
  $form['title']['#attributes'] = array(
       'onblur' => "if (this.value == '') {this.value = '".$form['title']['#value']."';} this.style.color = '#333333';",
         'onfocus' => "if (this.value == '".$form['title']['#value']."') {this.value = '';} this.style.color = '#333333';" );
  //Modify body text
  unset($form['body_field']['body']['#title']);

  $title = drupal_render($form['title']);
  $body = drupal_render($form['body_field']['body']);
  $form['body_field']['format']['#collapsed'] = FALSE;
  $form['body_field']['format']['#collapsible'] = FALSE;
  $form['body_field']['format']['#title'] = NULL;
  $body_filter = drupal_render($form['body_field']['format']);
  $tags = drupal_render($form['taxonomy']['tags']);
  //$published = drupal_render($form['options']['status']);
  $thematic_areas = drupal_render($form['taxonomy']['47']);
  $buttons = drupal_render($form['buttons']);
  $remaining_form = drupal_render($form);*/
  $personal = drupal_render($form['group_personal_information']);
  $professional = drupal_render($form['group_professional_information']);
  $social = drupal_render($form['group_social']);
  $everything_else = drupal_render($form);
  return $personal.$professional.$social.$everything_else;
/*  return '<table><tbody style="border:none;"><tr><td style="width:73%;"><div id="form-title">'.$title.'</div>'.'<div id="form-body">'.$body.'<div id="body-format">'.$body_filter.'</div></div><span style="display:none;">'.$remaining_form.'</span>'.'</td><td style="vertical-align:top;">'.$thematic_areas.$tags.'<div id="publish-post">'.$buttons.'</div>'."</td></tr></tbody></table>"; */
}


