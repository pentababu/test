<style>
.picture {
height:56px;
width:59px;
background:#fefefe;
border:2px solid #eff1ee;
-moz-border-radius: 3px;
 border-radius: 3px;
}
.picture img{
padding:4px 4px 4px 4px;
-moz-border-radius: 3px;
 border-radius: 3px;
}
#question-title a, a:hover, a:visited, a:focus, a:active {
color:#0e0e0e;
text-weight:bold;
font-size:14px;
text-decoration:none;
}

#question-container {
color:#0e0e0e;
margin-bottom:20px;
}
#name-link a{
color:#2398c9;text-align:center;width:59px;display:inline-block;text-weight:normal;
}
#question-meta{
display:inline-block;
font-size:11px;
color:#999;
margin-top:10px;
}
#question-meta img{
vertical-align:middle;
}
#question-meta em {
  font-style: normal;
}
.item-list ul li{
list-style-type:none;
display:inline-block;
}
.item-list a{
color:#2398c9;
font-size:11px;
font-style:normal;
}
.item-list{
background:url("/sites/all/themes/qollabsocial/images/tag.png") no-repeat;
margin-top:5px;
padding-left:20px;
}
#block-views-thematic_areas-block_1 a, a:visited, a:focus, a:active{
color:#417bb6;
font-size:11px;
padding:7px;
}
#block-views-thematic_areas-block_1 .field-content{
background:#f9f9f9;
-moz-border-radius: 2px 2px;
 border-radius: 2px 2px;
}
#block-views-thematic_areas-block_1 .views-row{
margin:3px;
}
</style>

<div id="question-container"><table><tbody style="border:none;"><tr><td style="vertical-align:top;"><?php print $fields['picture']->content;?><div id="name-link"><?php print $fields['name']->content;?></div></td><td style="padding-top:1px;padding-left:15px;"><div id="question-title"><?php print $fields['title']->content;?></div><div id="question-teaser"><?php print $fields['teaser']->content;?></div><div id="question-meta"><img src="/sites/all/themes/qollabsocial/images/time.png"/><span style="padding-left:5px;"><?php print $fields['created']->content;?></span><span style="padding-left:35px;"><img src="/sites/all/themes/qollabsocial/images/following.png"/><span style="padding-left:5px;"><?php print $fields['totalcount']->content." views";?></span></span><span style="padding-left:35px;"><img src="/sites/all/themes/qollabsocial/images/answer.png"/><?php print $fields['comment_count']->content." Answers";?></span></div><div class="item-list"><?php print $fields['tid']->content;?></div></td></tr></tbody></table></div>
